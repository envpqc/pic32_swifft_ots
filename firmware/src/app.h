/*******************************************************************************
*  MPLAB Harmony Application Header File
*
*  Company:
*   Microchip Technology Inc.
*
*  File Name:
*   app.h
*
*  Summary:
*   This header file provides prototypes and definitions for the application.
*
*  Description:
*   This header file provides function prototypes and data type definitions for
*   the application.  Some of these are required by the system (such as the
*   "APP_Initialize" and "APP_Tasks" prototypes) and some of them are only used
*   internally by the application (such as the "APP_STATES" definition).  Both
*   are defined here for convenience.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
*  Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.
*
*  Microchip licenses to you the right to use, modify, copy and distribute
*  Software only when embedded on a Microchip microcontroller or digital signal
*  controller that is integrated into your product or third party product
*  (pursuant to the sublicense terms in the accompanying license agreement).
*
*  You should refer to the license agreement accompanying this Software for
*  additional information regarding your rights and obligations.
*
*  SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
*  MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
*  IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
*  CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
*  OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
*  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
*  CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
*  SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
*  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
//DOM-IGNORE-END

#ifndef _APP_H
#define _APP_H

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include "system_config.h"
#include "system_definitions.h"
#include "crypto/crypto.h"
#include "rand_openssl_aes.h"

// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

extern "C" {
#endif
// DOM-IGNORE-END

#define SUCCESS    0
#define FAILURE    -1

// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application states
 *
 * Summary:
 *  Application states enumeration
 *
 * Description:
 *  This enumeration defines the valid application states.  These states
 *  determine the behavior of the application at various times.
 */
typedef enum
{
    /* Application's state machine's initial state */
    APP_STATE_INIT = 0,

    /* Performs the known answer test */
    APP_STATE_KNOWN_ANSWER_TEST,

    /* Initializes S-OTS */
    APP_STATE_INIT_SOTS,

    /* Performs the S-OTS Signature test */
    APP_STATE_SOTS_SIGNATURE_TEST,

    /* Application's error state; can't leave once here */
    APP_STATE_ERROR,

    /* State to handle switch 1 press */
    APP_STATE_HANDLE_SWITCH_1,

    /* State to handle switch 2 press */
    APP_STATE_HANDLE_SWITCH_2,

    /* State to handle switch 3 press */
    APP_STATE_HANDLE_SWITCH_3,

    /* Create the Timer Object */
    APP_STATE_TIMER_OBJECT_CREATE,

    /* Debounces the button press */
    APP_STATE_DEBOUNCE_START,

    /* Application's spin/idle state */
    APP_STATE_SPIN,

    APP_NUM_STATES
} APP_STATES;


// *****************************************************************************
/* Application Data
 *
 * Summary:
 *  Holds application data
 *
 * Description:
 *  This structure holds the application's data.
 *
 * Remarks:
 *  Application strings and buffers are be defined outside this structure.
 */
typedef struct
{
    /* The application's current state */
    APP_STATES     state;

    /* Heartbeat driver timer handle */
    DRV_HANDLE     heartbeatTimer;

    /* Heartbeat timer timeout count */
    unsigned int   heartbeatCount;

    /* Heartbeat LED toggle flag */
    bool           heartbeatToggle;

    /* Handle to the timer object  */
    SYS_TMR_HANDLE tmrObj;

    /* Change in switch detected */
    bool           changeNoticed;

    /* Flag indicating when the switch-1 has been pressed */
    bool           switch1HasBeenPressed;

    /* Flag indicating when the switch-2 has been pressed */
    bool           switch2HasBeenPressed;

    /* Flag indicating when the switch-3 has been pressed */
    bool           switch3HasBeenPressed;

    /* Button 1 de-bounce counter */
    unsigned int   debounce1Counter;

    /* Button 2 de-bounce counter */
    unsigned int   debounce2Counter;

    /* Button 3 de-bounce counter */
    unsigned int   debounce3Counter;

    /* Button timeout counter */
    unsigned int   timeoutCounter;
} APP_DATA;


// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Routines
// *****************************************************************************
// *****************************************************************************
/* These routines are called by drivers when certain events occur.
 */

/*******************************************************************************
 * Function:
 *  void APP_SwitchChangeNoticed ( void )
 *
 * Summary:
 *  Notifies the application that a change notice event occurred on the
 *  switch signal.
 *
 * Description:
 *  This function notifies the application that a change notice event occurred
 *  on the switch signal.  It also clears the CN mismatch and the interrupt
 *  source flag.
 *
 * Precondition:
 *  The change notice detection should be cleared and armed and a CN detection
 *  should have occurred.
 *
 * Parameters:
 *  None.
 *
 * Returns:
 *  None.
 *
 * Example:
 *  <code>
 *      APP_SwitchChangeNoticed();
 *  </code>
 *
 * Remarks:
 *  This routine can be used inside the CN ISR.
 */
void APP_SwitchChangeNoticed(void);

// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
 * Function:
 *  void APP_Initialize ( void )
 *
 * Summary:
 *   MPLAB Harmony application initialization routine.
 *
 * Description:
 *  This function initializes the Harmony application.  It places the
 *  application in its initial state and prepares it to run so that its
 *  APP_Tasks function can be called.
 *
 * Precondition:
 *  All other system initialization routines should be called before calling
 *  this routine (in "SYS_Initialize").
 *
 * Parameters:
 *  None.
 *
 * Returns:
 *  None.
 *
 * Example:
 *  <code>
 *  APP_Initialize();
 *  </code>
 *
 * Remarks:
 *  This routine must be called from the SYS_Initialize function.
 */
void APP_Initialize(void);


/*******************************************************************************
 * Function:
 *  void APP_Tasks ( void )
 *
 * Summary:
 *  MPLAB Harmony Demo application tasks function
 *
 * Description:
 *  This routine is the Harmony Demo application's tasks function.  It
 *  defines the application's state machine and core logic.
 *
 * Precondition:
 *  The system and application initialization ("SYS_Initialize") should be
 *  called before calling this.
 *
 * Parameters:
 *  None.
 *
 * Returns:
 *  None.
 *
 * Example:
 *  <code>
 *  APP_Tasks();
 *  </code>
 *
 * Remarks:
 *  This routine must be called from SYS_Tasks() routine.
 */
void APP_Tasks(void);

/*******************************************************************************
 * Function:
 *  bool APP_SetState ( APP_STATES newState )
 *
 * Summary:
 *  State transition function
 *
 * Description:
 *  Changes to the new state if transition is valid and prints out previous state and new state.
 *
 * Precondition:
 *  N/A
 *
 * Parameters:
 *  newState - The new state to be transitioned to
 *
 * Returns:
 *  TRUE:  requested state was set
 *  FALSE: requested state was not set
 *
 * Example:
 *  <code>
 *  APP_SetState(APP_STATE_SPIN);
 *  </code>
 *
 * Remarks:
 *  This routine must be called from APP_Tasks() routine.
 */
bool APP_SetState(APP_STATES newState);

/*******************************************************************************
 * Function:
 *  char * APP_GetStateName ( APP_STATES state )
 *
 * Summary:
 *  State name retrieval function
 *
 * Description:
 *  Returns the stringified state name
 *
 * Precondition:
 *  N/A
 *
 * Parameters:
 *  state - The new state whose name is be retrieved
 *
 * Returns:
 *  char *:  the stringified name of the state
 *
 * Example:
 *  <code>
 *  printf("State name = %s\n\r", APP_GetStateName(APP_STATE_SPIN));
 *  </code>
 *
 * Remarks:
 *  N/A
 */
char *APP_GetStateName(APP_STATES state);

/*******************************************************************************
 * Function:
 *  uint64_t APP_GetElapsedTime ( uint64_t startTime_ticks, char *eventName )
 *
 * Summary:
 *  Timer function
 *
 * Description:
 *  Changes to the new state if transition is valid and prints out previous state and new state.
 *
 * Precondition:
 *  N/A
 *
 * Parameters:
 *  uint64_t startTime_ticks - The start time in ticks from which to take the time diff
 *  char     *eventName      - The string to include in the printed out time diff
 *
 * Returns:
 *  uint64_t - The time diff in ticks
 *
 * Example:
 *  <code>
 *  APP_GetElapsedTime(appStartTime_ticks, "App from start to finish");
 *  </code>
 *
 * Remarks:
 *  N/A
 */
uint64_t APP_GetElapsedTime(uint64_t startTime_ticks, char *eventName);


#endif /* _APP_H */

//DOM-IGNORE-BEGIN
#ifdef __cplusplus
}
#endif
//DOM-IGNORE-END

/*******************************************************************************
 * End of File
 */

