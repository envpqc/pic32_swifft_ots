#ifndef _MULTIPRECISION_H_
#define _MULTIPRECISION_H_

#include "config.h"

#include <inttypes.h>   // PRIx32
#include <stddef.h>     // size_t

#if (TARGET == TARGET_AMD64)
    #define MAX_DIGIT           0xFFFFFFFFFFFFFFFFULL
    #define HIBITMASK           0x8000000000000000ULL
    #define BITS_PER_DIGIT      64
    #define PRIuBIGD            PRIu64
    #define PRIxBIGD            PRIx64
    #define PRIXBIGD            PRIX64
#elif (TARGET == TARGET_x86)
    #define MAX_DIGIT           0xFFFFFFFFUL
    #define HIBITMASK           0x80000000UL
    #define BITS_PER_DIGIT      32
    #define PRIuBIGD            PRIu32
    #define PRIxBIGD            PRIx32
    #define PRIXBIGD            PRIX32
#endif

#define MAX_FIXED_BIT_LENGTH    512
//#define MAX_FIXED_BIT_LENGTH    1024
#define MAX_FIXED_DIGITS        ((MAX_FIXED_BIT_LENGTH + BITS_PER_DIGIT - 1) / BITS_PER_DIGIT)
#define NWORDS_FIELD            (MAX_FIXED_DIGITS / 2)

/**************** Function prototypes ****************/
/************* Multiprecision functions **************/

// Multiprecision addition, c = a+b, where lng(a) = lng(b) = nwords. Returns the carry bit
unsigned int mpAdd(DIGIT_T *c, const DIGIT_T *a, const DIGIT_T *b, unsigned int nwords);

// Multiprecision subtraction, c = a-b, where lng(a) = lng(b) = nwords. Returns the borrow bit
unsigned int mpSubtract(DIGIT_T *c, const DIGIT_T *a, const DIGIT_T *b, unsigned int nwords);

// Digit multiplication, digit * digit -> 2-digit result
void digit_x_digit(DIGIT_T a, DIGIT_T b, DIGIT_T *c);

/** Computes product w = u * v
 * @param[out] w To receive the product, an array of size 2 x `nwords`
 * @param[in] u An array of size `nwords`
 * @param[in] v An array of size `nwords`
 * @param[in] nwords size of arrays `u` and `v`
 * @pre `w` and `u` must not overlap.
 * @warning The product must be of size 2 x `nwords`
 */
//int mpMultiply(DIGIT_T w[], const DIGIT_T u[], const DIGIT_T v[], size_t nwords);

void mpMultiply(DIGIT_T *c, const DIGIT_T *a, const DIGIT_T *b, unsigned int nwords);

/** Computes integer division of u by v such that u=qv+r
 * @param[out] q to receive quotient = u div v, an array of size `udigits`
 * @param[out] r to receive divisor = u mod v, an array of size `udigits`
 * @param[in]  u dividend of size `udigits`
 * @param[in] udigits size of arrays `q` `r` and `u`
 * @param[in]  v divisor of size `vdigits`
 * @param[in] vdigits size of array `v`
 * @pre `q` and `r` must be independent of `u` and `v`.
 * @warning Trashes q and r first
 */
int mpDivide(DIGIT_T q[], DIGIT_T r[], const DIGIT_T u[], size_t udigits, DIGIT_T v[], size_t vdigits);

// Zeroing a field element, a = 0
void mpSetZero(DIGIT_T *a, unsigned int nwords);

// Sets a = d where d is a single digit
void mpSetDigit(DIGIT_T *a, DIGIT_T d, unsigned int nwords);

// Multiprecision left shift
void mpShiftLeftInPlace(DIGIT_T *x, unsigned int shift, unsigned int nwords);

// Multiprecision OR
void mpOrBits(DIGIT_T a[], const DIGIT_T b[], const DIGIT_T c[], size_t nwords);

// Sets a = b
void mpSetEqual(DIGIT_T a[], const DIGIT_T b[], size_t nwords);

// Returns sign of (a - b) as 0, +1 or -1. Not constant-time.
int mpCompare(const DIGIT_T a[], const DIGIT_T b[], size_t nwords);

/** Returns true if a is zero, else false
 *  @remark Not constant-time.
 */
int mpIsZero(const DIGIT_T a[], size_t nwords);

/** Computes p = x * y, where x and y are single digits */
int spMultiply(DIGIT_T p[2], DIGIT_T x, DIGIT_T y);

/** Computes quotient q = u div v, remainder r = u mod v, where q, r and v are single digits */
DIGIT_T spDivide(DIGIT_T *q, DIGIT_T *r, const DIGIT_T u[2], DIGIT_T v);

///** Computes the inverse of `u` modulo `m`, inv = u^{-1} mod v */
//int mpModInv(DIGIT_T inv[], const DIGIT_T u[], const DIGIT_T v[], size_t nwords);

/** Returns number of significant non-zero digits in a */
size_t mpSizeof(const DIGIT_T a[], size_t nwords);

/** Computes quotient q = u div d, returns remainder */
DIGIT_T mpShortDiv(DIGIT_T q[], const DIGIT_T u[], DIGIT_T d, size_t nwords);

/**********************/
/* BITWISE OPERATIONS */
/**********************/

/** Returns number of significant bits in a */
size_t mpBitLength(const DIGIT_T a[], size_t ndigits);

/** Computes a = b << x */
DIGIT_T mpShiftLeft(DIGIT_T a[], const DIGIT_T b[], size_t x, size_t nwords);

/** Computes a = b >> x */
DIGIT_T mpShiftRight(DIGIT_T a[], const DIGIT_T b[], size_t x, size_t nwords);

/*******************/
/* PRINT UTILITIES */
/*******************/

/** Print in decimal format with optional prefix and suffix strings */
void mpPrintDecimal(const char *prefix, const DIGIT_T *a, size_t ndigits, const char *suffix);

/** Print in hex format with optional prefix and suffix strings */
void mpPrintHex(const char *prefix, const DIGIT_T *a, size_t nwords, const char *suffix);

/************************/
/* CONVERSION UTILITIES */
/************************/

/** Converts big digit a into string of octets, in big-endian order, padding to nbytes or truncating if necessary.
 * @returns number of non-zero octets required. */
size_t mpConvToOctets(const DIGIT_T a[], size_t ndigits, unsigned char *c, size_t nbytes);

/** Converts big digit a into a string in decimal format, where s has size smax including the terminating zero.
 * @returns number of chars required excluding leading zeroes. */
size_t mpConvToDecimal(const DIGIT_T a[], size_t ndigits, char *s, size_t smax);

#endif
