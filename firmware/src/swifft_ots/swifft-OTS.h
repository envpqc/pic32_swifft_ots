#ifndef _SWIFFT_OTS_H_
#define _SWIFFT_OTS_H_

/**
 * Inits S-OTS Params
 */
int initSOTS(sots_params *params,
             BitSequence pk[E_CONSTANT][N_CONSTANT], size_t pk_len,
             BitSequence sk[E_CONSTANT][M_CONSTANT][N_CONSTANT], size_t sk_len,
             bool useDeterministicData);

/**
 * Performs S-OTS Known Answer Test
 */
int performKnownAnswerTest(sots_params *params,
                           BitSequence pk[E_CONSTANT][N_CONSTANT], size_t pk_len,
                           BitSequence sk[E_CONSTANT][M_CONSTANT][N_CONSTANT], size_t sk_len);

/**
 * Performs S-OTS Signature Test
 */
int perform_SOTS_SignatureTest(sots_params *params,
                               BitSequence pk[E_CONSTANT][N_CONSTANT], size_t pk_len,
                               BitSequence sk[E_CONSTANT][M_CONSTANT][N_CONSTANT], size_t sk_len,
                               bool useDeterministicData);

#endif
