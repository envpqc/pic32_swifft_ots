#ifndef _SOTS_CORE_H_
#define _SOTS_CORE_H_

#include "params.h"     // sots_params, BitSequence

#include <stdbool.h>    // bool

int sots_core_generate_keypair(sots_params *params,
                               BitSequence pk[E_CONSTANT][N_CONSTANT], size_t pk_len,
                               BitSequence sk[E_CONSTANT][M_CONSTANT][N_CONSTANT], size_t sk_len,
                               bool useDeterministicData);

int sots_core_sign(const sots_params *params,
                   BitSequence sk[E_CONSTANT][M_CONSTANT][N_CONSTANT], size_t sk_len,
                   BitSequence sigma[M_CONSTANT][N_CONSTANT], size_t sigma_len);

int sots_core_verify_signature(const sots_params *params,
                               BitSequence sigma[M_CONSTANT][N_CONSTANT], size_t sigma_len,
                               BitSequence pk[E_CONSTANT][N_CONSTANT], size_t pk_len);

#endif
