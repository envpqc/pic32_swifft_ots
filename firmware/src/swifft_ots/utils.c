#include "utils.h"

#include <stdio.h>      // printf, sprintf
#include <stdlib.h>     // EXIT_SUCCESS, EXIT_FAILURE

/**
 * Converts the value of 'in' to 'outlen' bytes in big-endian byte order.
 */
void ull_to_bytes(unsigned char *out, unsigned int outlen,
                  unsigned long long in)
{
    int i;

    /* Iterate over out in decreasing order, for big-endianness. */
    for (i = (int)outlen - 1; i >= 0; i--)
    {
        out[i] = (unsigned char)(in & 0xFFu);
        in     = in >> 8u;
    }
}

void extendedEuclid(long *result, long a, long b)
{
    if (b == 0)
    {
        result[0] = a;
        result[1] = 1;
        result[2] = 0;
    }
    else
    {
        int t, t2;
        extendedEuclid(result, b, a % b);
        t         = (int)result[1];
        t2        = (int)result[2];
        result[1] = result[2];
        result[2] = t - (a / b * t2);
    }
}

long modinv(long a, long m)
{
    long retVal = -1;
    long result[3];

    extendedEuclid(result, a, m);

    if (result[0] != 1)
    {
        printf("Inverse doesn't exist for a=%ld and m=%ld\n\r", a, m);
    }
    else
    {
        // m is added to handle negative x
        retVal = (result[1] % m + m) % m;
    }

    return retVal;
}

unsigned int bit_reverse(unsigned int input, unsigned int numOfBits)
{
    unsigned int reverse_num = 0;
    unsigned int i;

    for (i = 0; i < numOfBits; i++)
    {
        if ((input & (1u << i)))
        {
            reverse_num |= 1u << ((numOfBits - 1u) - i);
        }
    }

    return reverse_num;
}

#if defined(PERFORM_CONSTANTS_COMPUTATION) && (TARGET == TARGET_x86)
// result = combi(n, r)
// JMR Note - Use MAX_FIXED_DIGITS vice NWORDS_FIELD to allow for larger temporary results
int mp_combi(DIGIT_T result[], unsigned int n, unsigned int r)
{
    unsigned int i;
    int          retCode = EXIT_SUCCESS;
    DIGIT_T      tempResult[MAX_FIXED_DIGITS];
    DIGIT_T      previousResult[MAX_FIXED_DIGITS];
    DIGIT_T      i_bigDigit[MAX_FIXED_DIGITS];
    DIGIT_T      remainder[MAX_FIXED_DIGITS];

    if (n < r)
    {
        mpSetDigit(tempResult, 0, MAX_FIXED_DIGITS);

        return EXIT_SUCCESS;
    }
    else
    {
        mpSetDigit(tempResult, 1, MAX_FIXED_DIGITS);
    }

    // Clear out big digits
    mpSetZero(i_bigDigit, MAX_FIXED_DIGITS);
    mpSetZero(remainder, MAX_FIXED_DIGITS);

    if (r > n / 2)
    {
        r = n - r; // because C(n, r) == C(n, n - r)
    }

    for (i = 1; i <= r; i++)
    {
        // previousResult = tempResult
        mpSetEqual(previousResult, tempResult, MAX_FIXED_DIGITS);

        // i_bigDigit = n - r + i
        mpSetDigit(i_bigDigit, n - r + i, MAX_FIXED_DIGITS / 2);

        // tempResult = tempResult * (n - r + i);
        mpMultiply(tempResult, previousResult, i_bigDigit, MAX_FIXED_DIGITS / 2);

        // previousResult = tempResult
        mpSetEqual(previousResult, tempResult, MAX_FIXED_DIGITS);

        // i_bigDigit = i
        mpSetDigit(i_bigDigit, (DIGIT_T)i, MAX_FIXED_DIGITS / 2);

        // tempResult = tempResult / i, remainder = tempResult % i;
        mpDivide(tempResult, remainder, previousResult, MAX_FIXED_DIGITS, i_bigDigit, MAX_FIXED_DIGITS / 2);
    }

    // previousResult = tempResult
    mpSetEqual(result, tempResult, NWORDS_FIELD);

    return retCode;
}
#endif

unsigned long power(unsigned long base, unsigned long exp)
{
    if (exp == 0)
    {
        return 1;
    }
    else if (exp % 2)
    {
        return base * power(base, exp - 1);
    }
    else
    {
        unsigned long temp = power(base, exp / 2);

        return temp * temp;
    }
}

//function modular_pow(base, exponent, modulus)
//    if modulus = 1 then return 0
//    c := 1
//    for e_prime = 0 to exponent-1
//        c := (c * base) mod modulus
//    return c
unsigned long modular_power(unsigned long base, unsigned long exponent, unsigned long modulus)
{
    unsigned long c = 1;
    unsigned long e_prime;

    if (modulus == 1)
    {
        return 0;
    }

    for (e_prime = 0u; e_prime < exponent; e_prime++)
    {
        c = (c * base) % modulus;
    }

    return c;
}
