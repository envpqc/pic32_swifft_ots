#ifndef _SOTS_PARAMS_H_
#define _SOTS_PARAMS_H_

#include "config.h"
#include "multiprecision.h"

#include <stdbool.h>    // bool

/* Constants */
#define L_CONSTANT            224u
#define E_CONSTANT            229u
#define K_CONSTANT            107u
#define M_CONSTANT            16u
#define N_CONSTANT            64u
#define q_CONSTANT            257       // Note:  must be signed
#define OMEGA_CONSTANT        42u
#define OMEGA_INV_CONSTANT    153u
//static const unsigned int NUM_ROWS  = E_CONSTANT - K_CONSTANT + 3 - 1;
//static const unsigned int SOTS_MLEN = L_CONSTANT / CHAR_BIT;
// JMR Note - use of the above static const doesn't work with the PIC32 compiler
#define NUM_ROWS     124u
#define SOTS_MLEN    28u

typedef uint16_t BitSequence;

/* This structure will be populated when calling sots_init. */
typedef struct
{
    BitSequence OMEGA_inv;
    BitSequence psi_rev[N_CONSTANT];
    BitSequence psi_inv_rev[N_CONSTANT];
    DIGIT_T     first_row[NUM_ROWS][NWORDS_FIELD];
    DIGIT_T     eChooseK[NWORDS_FIELD];
    DIGIT_T     m_bigInt[NWORDS_FIELD];
    BitSequence A[M_CONSTANT][N_CONSTANT];
} sots_params;

/**
 * Configures params for SWIFFT-OTS
 */
int configure_SOTS_Params(sots_params *params, bool useDeterministicData);

/**
 * Releases dynamic memory in params for SWIFFT-OTS
 */
void sots_cleanup(sots_params *params);

/**
 * Randomizes A, the primitive Fourier coefficients of the fixed multipliers
 */
void randomizeA(sots_params *params);

/**
 * Randomizes m, the data to be signed
 */
#ifdef RANDOMIZE_M_DATA
void randomizeM(sots_params *params);
#endif

/***********************************************************************
*                          Constant Data
***********************************************************************/

#include "sots_constant_data.h"

#endif
