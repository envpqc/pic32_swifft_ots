#include "config.h"
#include "randombytes.h"
#include "sots_core.h"
#include "swifft.h"

#include <stdio.h>      // printf
#include <stdlib.h>     // EXIT_SUCCESS, EXIT_FAILURE
#include <string.h>     // memset, memcpy

/*
 * Generates the secret key sk and public key pk
 *
 * Python:
 * def keygen():
 *     sk = [[[random.randint(0,1) for i in range(N)] for j in range(M)] for k in range(E)]
 *     pk = [SWIFFT(A, sk[i]) for i in range(E)]
 *     return [pk, sk]
 */
int sots_core_generate_keypair(sots_params *params,
                               BitSequence pk[E_CONSTANT][N_CONSTANT], size_t pk_len,
                               BitSequence sk[E_CONSTANT][M_CONSTANT][N_CONSTANT], size_t sk_len,
                               bool useDeterministicData)
{
    int          result = EXIT_SUCCESS;
    unsigned int i;

#ifdef PERFORM_ARRAY_DIMENSION_VALIDATION
    // Validate dimensions
    if (pk_len != E_CONSTANT)
    {
        printf("ERROR: pk_len not equal to expected size of %d; actual value = %u\n\r",
               E_CONSTANT,
               pk_len);

        return EXIT_FAILURE;
    }
    if (sk_len != E_CONSTANT)
    {
        printf("ERROR: sk_len not equal to expected size of %d; actual value = %u\n\r",
               E_CONSTANT,
               sk_len);

        return EXIT_FAILURE;
    }
#endif

    if (useDeterministicData)
    {
        // Initialize SK to static values for testing
        memcpy(sk, &SK_DEFAULT, sk_len * sizeof(sk[0]));
    }
    else
    {
        // SK_DEFAULT[E_CONSTANT][M_CONSTANT][N_CONSTANT]
        // Note that srand() should be called just once in the main() function
        for (i = 0; i < E_CONSTANT; i++)
        {
            unsigned int j;

            for (j = 0; j < M_CONSTANT; j++)
            {
                unsigned int k;

                for (k = 0; k < N_CONSTANT; k++)
                {
                    sk[i][j][k] = (BitSequence)(RANDOM8 % 2);
                }
            }
        }

#ifdef RANDOMIZE_A_DATA
        randomizeA(params);
#endif
    }

    // Generate PK
    for (i = 0; i < E_CONSTANT; i++)
    {
        SWIFFT(params,
               sk[i], sizeof(sk[i]) / sizeof(sk[i][0]),
               pk[i], sizeof(pk[i]) / sizeof(pk[i][0]));
    }

    return result;
}

/**
 * Signs the L-bit message m using the secret key sk. Returns the sum of sk[i] for i in the k-subset of {0,...,E-1}
 * returned by new_enumerate
 *
 * Python:
 * def sign(m, sk):
 *     subsetE = new_enumerate(m)
 *     print subsetE
 *     sigma = [[0 for i in range(N)] for j in range(M)]
 *     for k in range(M):
 *         for j in range(N):
 *             for i in range(K):
 *                 sigma[k][j] += sk[subsetE[i]][k][j]
 *     return sigma
 */
int sots_core_sign(const sots_params *params,
                   BitSequence sk[E_CONSTANT][M_CONSTANT][N_CONSTANT], size_t sk_len,
                   BitSequence sigma[M_CONSTANT][N_CONSTANT], size_t sigma_len)
{
    int          result = EXIT_SUCCESS;
    unsigned int i, j, k;

#ifdef PERFORM_ARRAY_DIMENSION_VALIDATION
    // Validate dimensions
    if (sk_len != E_CONSTANT)
    {
        printf("ERROR: sk_len not equal to expected size of %d; actual value = %u\n\r",
               E_CONSTANT,
               sk_len);

        return EXIT_FAILURE;
    }
    if (sigma_len != M_CONSTANT)
    {
        printf("ERROR: sm_len not equal to expected size of %d; actual value = %u\n\r",
               M_CONSTANT,
               sigma_len);

        return EXIT_FAILURE;
    }
#endif

    BitSequence subsetE[K_CONSTANT];

    if (new_enumerate(params, subsetE, sizeof(subsetE) / sizeof(subsetE[0])) != EXIT_SUCCESS)
    {
        printf("ERROR: new_enumerate failed while in sots_core_sign\n\r");

        return EXIT_FAILURE;
    }

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= MEDIUM_VERBOSITY
    printf("subsetE = { %3u", subsetE[0]);
    for (i = 1; i < K_CONSTANT; i++)
    {
        printf(", %3u", subsetE[i]);
    }
    printf("}\n\r");
#endif

    memset(sigma, 0x0, sigma_len * sizeof(sigma[0]));

    for (k = 0; k < M_CONSTANT; k++)
    {
        for (j = 0; j < N_CONSTANT; j++)
        {
            for (i = 0; i < K_CONSTANT; i++)
            {
                sigma[k][j] += sk[subsetE[i]][k][j];
            }
        }
    }

    return result;
}

/**
 * Verifies that sigma is a valid signature of message m with public key pk
 *
 * Python:
 * def verify(m, sigma, pk):
 *     # Check that the entries of sigma are in [0,K]
 *     for i in range(N):
 *         for j in range(M):
 *             if sigma[j][i] > K:
 *                 print "Signature not valid: values out of range."
 *                 return False
 *     # Compute SWIFFT(sigma)
 *     fsigma = SWIFFT(A, sigma)
 *     subsetE = new_enumerate(m)
 *     # Check that SWIFFT(sigma) equals the sum of the corresponding pk entries
 *     for i in range(N):
 *         sum_val = sum([pk[subsetE[j]][i] for j in range(K)])%q
 *         if fsigma[i] != sum_val:
 *             print "Signature not valid: ", i, "-th entry incorrect."
 *             print fsigma[i], " != ", sum_val
 *             return False
 *     print "Signature valid."
 *     return True
 */
int sots_core_verify_signature(const sots_params *params,
                               BitSequence sigma[M_CONSTANT][N_CONSTANT], size_t sigma_len,
                               BitSequence pk[E_CONSTANT][N_CONSTANT], size_t pk_len)
{
    int          result;
    unsigned int i, j;
    BitSequence  fsigma[N_CONSTANT];

#ifdef PERFORM_ARRAY_DIMENSION_VALIDATION
    // Validate dimensions
    if (sigma_len != M_CONSTANT)
    {
        printf("ERROR: sigma_len not equal to expected size of %d; actual value = %u\n\r",
               M_CONSTANT,
               sigma_len);

        return EXIT_FAILURE;
    }
    if (pk_len != E_CONSTANT)
    {
        printf("ERROR: pk_len not equal to expected size of %d; actual value = %u\n\r",
               E_CONSTANT,
               pk_len);

        return EXIT_FAILURE;
    }
#endif

    // Check that the entries of sigma are in [0,K]
    /* Python:
     * for i in range(N):
     *  for j in range(M):
     *      if sigma[j][i] > K:
     *          print "Signature not valid: values out of range."
     *          return False
     */
    for (i = 0; i < M_CONSTANT; i++)
    {
        for (j = 0; j < N_CONSTANT; j++)
        {
            if (sigma[i][j] > K_CONSTANT)
            {
                printf("ERROR:  Signature is not valid:  values out of range\n\r");

                return EXIT_FAILURE;
            }
        }
    }

    memset(fsigma, 0x0, sizeof(fsigma));

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= HIGH_VERBOSITY
    printf("Before last SWIFFT - fsigma: %u", fsigma[0]);
    for (i = 1; i < N_CONSTANT; i++)
    {
        printf(", %u", fsigma[i]);
    }
    printf("\n\r");
#endif

    // Compute SWIFFT(sigma)
    // Python:  fsigma = SWIFFT(A, sigma)
    SWIFFT(params,
           sigma, sigma_len,
           fsigma, sizeof(fsigma) / sizeof(fsigma[0]));

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= HIGH_VERBOSITY
    printf("After  last SWIFFT - fsigma: %u", fsigma[0]);
    for (i = 1; i < N_CONSTANT; i++)
    {
        printf(", %u", fsigma[i]);
    }
    printf("\n\r");
#endif

    BitSequence subsetE[K_CONSTANT];

    // Python:  subsetE = new_enumerate(m)
    result = new_enumerate(params, subsetE, sizeof(subsetE) / sizeof(subsetE[0]));

    for (i = 0; i < N_CONSTANT; i++)
    {
        unsigned int sum_val = 0;

        // Python:  sum_val = sum([pk[subsetE[j]][i] for j in range(K)]) % q
        for (j = 0; j < K_CONSTANT; j++)
        {
            sum_val += pk[subsetE[j]][i];
        }
        sum_val = sum_val % q_CONSTANT;

        // Python:  if fsigma[i] != sum_val:
        if (fsigma[i] != sum_val)
        {
            printf("Signature is NOT valid! %u-th entry is incorrect\n\r", i);
            printf("fsigma[%u] (%u) != sum_val (%u)\n\r", i, fsigma[i], sum_val);

            return EXIT_FAILURE;
        }
    }

    return result;
}
