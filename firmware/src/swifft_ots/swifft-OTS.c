/*
 * ============================================================================
 * Name        : swifft-OTS.c
 * Author      : John Ratz
 * Version     : 1.0
 * Copyright   : TBD
 * Description : C code for SWIFFT-based One-Time Signature
 * ============================================================================
 */

#include "config.h"
#include "params.h"
#include "randombytes.h"
#include "sots_core.h"
#include "swifft-OTS.h"
#include "utils.h"

#include <stdio.h>      // printf

/*
 * Macros
 */
#define StartTestTimer(tEarlier)              tEarlier = SYS_TMR_TickCountGetLong()
#define EndTestTimer(tDiff_usec, tEarlier)    do {                       \
        tDiff_usec = (double)(SYS_TMR_TickCountGetLong() - tEarlier) / tickFrequencyHz * SEC_TO_USEC; \
} while (0)

/*
 * Constants
 */
static const double SEC_TO_USEC               = 1000.0 * 1000.0;
static const double MSEC_TO_USEC              = 1000.0;
static const double SEC_TO_MSEC               = 1000.0;
static const double USEC_TO_SEC               = 1.0 / (1000.0 * 1000.0);
static const double USEC_TO_MSEC              = 1.0 / 1000.0;

/*
 * Static variables to keep track of keypair generation, signature creation, and verification times
 */
static double    totalKeypairGenTimeDiff_usec = 0.0;
static double    totalSignTimeDiff_usec       = 0.0;
static double    totalVerifyTimeDiff_usec     = 0.0;
static double    timeDiff_usec                = 0.0;
static double    tickFrequencyHz              = 0.0;
static uint64_t  testEarlier                  = 0;

/*
 * Generates the Secret and Public Keypair
 */
int generateKeypair(sots_params *params,
                    BitSequence pk[E_CONSTANT][N_CONSTANT], size_t pk_len,
                    BitSequence sk[E_CONSTANT][M_CONSTANT][N_CONSTANT], size_t sk_len,
                    bool useDeterministicData)
{
    int          result;

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= ULTRA_VERBOSITY
    unsigned int i, j;
#endif

    StartTestTimer(testEarlier);
    result = sots_core_generate_keypair(params,
                                        pk, pk_len,
                                        sk, sk_len,
                                        useDeterministicData);
    EndTestTimer(timeDiff_usec, testEarlier);
    totalKeypairGenTimeDiff_usec += timeDiff_usec;

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= LOW_VERBOSITY
    printf("Time spent generating keypair:             %f seconds; %f msec; %f usec\n\r",
           timeDiff_usec * USEC_TO_SEC,
           timeDiff_usec * USEC_TO_MSEC,
           timeDiff_usec);
#endif

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= ULTRA_VERBOSITY
    printf("Public key after sots_core_generate_keypair:\n\r");
    for (i = 0; i < E_CONSTANT; i++)
    {
        printf("pk[%03u]:  { %3u", i, pk[i][0]);
        for (j = 1; j < N_CONSTANT; j++)
        {
            printf(", %3u", pk[i][j]);
        }
        printf("}\n\r");
    }
#endif

    return result;
}

int createSignature(const sots_params *params,
                    BitSequence sk[E_CONSTANT][M_CONSTANT][N_CONSTANT], size_t sk_len,
                    BitSequence sigma[M_CONSTANT][N_CONSTANT], size_t sigma_len)
{
#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= MEDIUM_VERBOSITY
    unsigned int i;
#endif

    StartTestTimer(testEarlier);
    if (sots_core_sign(params,
                       sk, sk_len,
                       sigma, sigma_len) != EXIT_SUCCESS)
    {
        printf("ERROR:  Detected failure in sots_core_sign. Exiting...\n\r");

        return EXIT_FAILURE;
    }
    EndTestTimer(timeDiff_usec, testEarlier);
    totalSignTimeDiff_usec += timeDiff_usec;

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= LOW_VERBOSITY
    printf("Time spent in sots_core_sign:              %f seconds; %f msec; %f usec\n\r",
           timeDiff_usec * USEC_TO_SEC,
           timeDiff_usec * USEC_TO_MSEC,
           timeDiff_usec);
#endif

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= MEDIUM_VERBOSITY
    for (i = 0; i < M_CONSTANT; i++)
    {
        unsigned int j;

        printf("SIGMA[ %02u ] =  [%u", i, sigma[i][0]);

        for (j = 1; j < N_CONSTANT; j++)
        {
            printf(", %u", sigma[i][j]);
        }

        printf("]\n\r");
    }
#endif

    return EXIT_SUCCESS;
}

int verifySignature(const sots_params *params,
                    BitSequence sigma[M_CONSTANT][N_CONSTANT], size_t sigma_len,
                    BitSequence pk[E_CONSTANT][N_CONSTANT], size_t pk_len)
{
    StartTestTimer(testEarlier);
    if (sots_core_verify_signature(params,
                                   sigma, sigma_len,
                                   pk, pk_len) != EXIT_SUCCESS)
    {
        printf("ERROR:  Detected failure in sots_core_verify_signature\n\r");

        return EXIT_FAILURE;
    }
    EndTestTimer(timeDiff_usec, testEarlier);
    totalVerifyTimeDiff_usec += timeDiff_usec;

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= LOW_VERBOSITY
    printf("Time spent in sots_core_verify_signature:  %f seconds; %f msec; %f usec\n\r",
           timeDiff_usec * USEC_TO_SEC,
           timeDiff_usec * USEC_TO_MSEC,
           timeDiff_usec);
#endif

    return EXIT_SUCCESS;
}

/*
 * Performs a single signature creation and verification
 */
int performSingleSignatureTest(sots_params *params,
                               BitSequence pk[E_CONSTANT][N_CONSTANT], size_t pk_len,
                               BitSequence sk[E_CONSTANT][M_CONSTANT][N_CONSTANT], size_t sk_len,
                               bool useDeterministicData)
{
    BitSequence sigma[M_CONSTANT][N_CONSTANT];

#ifdef PERFORM_ARRAY_DIMENSION_VALIDATION
    // Validate dimensions
    if (pk_len != E_CONSTANT)
    {
        printf("ERROR: pk_len not equal to expected size of %u; actual value = %zu Exiting...\n\r",
               E_CONSTANT,
               pk_len);

        return EXIT_FAILURE;
    }
    if (sk_len != E_CONSTANT)
    {
        printf("ERROR: sk_len not equal to expected size of %u; actual value = %zu. Exiting...\n\r",
               E_CONSTANT,
               sk_len);

        return EXIT_FAILURE;
    }
#endif

    // Randomize the data to be signed
#ifdef RANDOMIZE_M_DATA
    randomizeM(params);
#endif

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= LOW_VERBOSITY
    mpPrintHex("\n\rSigning m = 0x", params->m_bigInt, NWORDS_FIELD, "\n\r");
#endif

#ifdef REGENERATE_KEYPAIR
    if (generateKeypair(
            params,
            pk, pk_len,
            sk, sk_len,
            useDeterministicData) != EXIT_SUCCESS)
    {
        printf("ERROR:  Failed to create keypair. Exiting...\n\r");

        return EXIT_FAILURE;
    }
#endif

    if (createSignature(params,
                        sk, sk_len,
                        sigma, sizeof(sigma) / sizeof(sigma[0])) != EXIT_SUCCESS)
    {
        printf("ERROR:  Failed to create signature. Exiting...\n\r");

        return EXIT_FAILURE;
    }

    if (verifySignature(params,
                        sigma, sizeof(sigma) / sizeof(sigma[0]),
                        pk, pk_len) != EXIT_SUCCESS)
    {
        printf("ERROR:  Failed to verify signature. Exiting...\n\r");

        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

/*
 * Prints out the final time report
 */
void printTimeReport(double totalTimeDiff_usec)
{
    printf("\n\r");
#ifndef REGENERATE_KEYPAIR  // Only generated the keypair once
    printf("Time spent in sots_core_generate_keypair:                                %f seconds; %f msec; %f usec\n\r",
           totalKeypairGenTimeDiff_usec * USEC_TO_SEC,
           totalKeypairGenTimeDiff_usec * USEC_TO_MSEC,
           totalKeypairGenTimeDiff_usec);
#else
    printf("Average time spent in sots_core_generate_keypair across %d signatures:  %f seconds; %f msec; %f usec\n\r",
           SOTS_SIGNATURES,
           totalKeypairGenTimeDiff_usec * USEC_TO_SEC / SOTS_SIGNATURES,
           totalKeypairGenTimeDiff_usec * USEC_TO_MSEC / SOTS_SIGNATURES,
           totalKeypairGenTimeDiff_usec / SOTS_SIGNATURES);
    printf("Total   time spent in sots_core_generate_keypair across %d signatures:  %f seconds; %f msec; %f usec\n\r",
           SOTS_SIGNATURES,
           totalKeypairGenTimeDiff_usec * USEC_TO_SEC,
           totalKeypairGenTimeDiff_usec * USEC_TO_MSEC,
           totalKeypairGenTimeDiff_usec);
#endif

    printf("Average time spent in sots_core_sign across %d signatures:              %f seconds; %f msec; %f usec\n\r",
           SOTS_SIGNATURES,
           totalSignTimeDiff_usec * USEC_TO_SEC / SOTS_SIGNATURES,
           totalSignTimeDiff_usec * USEC_TO_MSEC / SOTS_SIGNATURES,
           totalSignTimeDiff_usec / SOTS_SIGNATURES);
    printf("Total   time spent in sots_core_sign across %d signatures:              %f seconds; %f msec; %f usec\n\r",
           SOTS_SIGNATURES,
           totalSignTimeDiff_usec * USEC_TO_SEC,
           totalSignTimeDiff_usec * USEC_TO_MSEC,
           totalSignTimeDiff_usec);

    printf("Average time spent in sots_core_verify_signature across %d signatures:  %f seconds; %f msec; %f usec\n\r",
           SOTS_SIGNATURES,
           totalVerifyTimeDiff_usec * USEC_TO_SEC / SOTS_SIGNATURES,
           totalVerifyTimeDiff_usec * USEC_TO_MSEC / SOTS_SIGNATURES,
           totalVerifyTimeDiff_usec / SOTS_SIGNATURES);
    printf("Total   time spent in sots_core_verify_signature across %d signatures:  %f seconds; %f msec; %f usec\n\r",
           SOTS_SIGNATURES,
           totalVerifyTimeDiff_usec * USEC_TO_SEC,
           totalVerifyTimeDiff_usec * USEC_TO_MSEC,
           totalVerifyTimeDiff_usec);

    printf("Average time per signature over %d signatures:                          %f seconds; %f msec; %f usec\n\r",
           SOTS_SIGNATURES,
           totalTimeDiff_usec * USEC_TO_SEC / SOTS_SIGNATURES,
           totalTimeDiff_usec * USEC_TO_MSEC / SOTS_SIGNATURES,
           totalTimeDiff_usec / SOTS_SIGNATURES);
    printf("Total time spent performing %d signatures:                              %f seconds; %f msec; %f usec\n\r",
           SOTS_SIGNATURES,
           totalTimeDiff_usec * USEC_TO_SEC,
           totalTimeDiff_usec * USEC_TO_MSEC,
           totalTimeDiff_usec);
}

int initSOTS(sots_params *params,
             BitSequence pk[E_CONSTANT][N_CONSTANT], size_t pk_len,
             BitSequence sk[E_CONSTANT][M_CONSTANT][N_CONSTANT], size_t sk_len,
             bool useDeterministicData)
{
    if (configure_SOTS_Params(params, useDeterministicData) != EXIT_SUCCESS)
    {
        printf("ERROR:  Failed to configuring SOTS parameters. Exiting...\n\r");

        return EXIT_FAILURE;
    }

#ifdef REGENERATE_KEYPAIR
    printf("Note that the keypair will be generated before each signature.\n\r");
#else
    printf("Note that the keypair will be generated only once and will be reused for each signature.\n\r");

    if (generateKeypair(params,
                        pk, pk_len,
                        sk, sk_len,
                        useDeterministicData) != EXIT_SUCCESS)
    {
        printf("ERROR:  Failed to create keypair. Exiting...\n\r");

        return EXIT_FAILURE;
    }
#endif

    return EXIT_SUCCESS;
}

/*
 * Performs known answer test
 */
int performKnownAnswerTest(sots_params *params,
                           BitSequence pk[E_CONSTANT][N_CONSTANT], size_t pk_len,
                           BitSequence sk[E_CONSTANT][M_CONSTANT][N_CONSTANT], size_t sk_len)
{
    BitSequence  sigma[M_CONSTANT][N_CONSTANT];
    bool         useDeterministicData = true;

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= MEDIUM_VERBOSITY
    unsigned int i;
#endif

    tickFrequencyHz = SYS_TMR_TickCounterFrequencyGet();

    printf("Performing Known Answer Test...\n\r\n\r");

    StartTestTimer(testEarlier);

    if (configure_SOTS_Params(params, useDeterministicData) != EXIT_SUCCESS)
    {
        printf("ERROR:  Failed to configuring SOTS parameters. Exiting...\n\r");

        return EXIT_FAILURE;
    }

    // Generate keypair
    if (sots_core_generate_keypair(params,
                                   pk, pk_len,
                                   sk, sk_len,
                                   useDeterministicData) != EXIT_SUCCESS)
    {
        printf("ERROR:  Failed to create keypair. Exiting...\n\r");

        return EXIT_FAILURE;
    }

    // Create signature
    if (sots_core_sign(params,
                       sk, sk_len,
                       sigma, sizeof(sigma) / sizeof(sigma[0])) != EXIT_SUCCESS)
    {
        printf("ERROR in KAT:  Detected failure in sots_core_sign. Exiting...\n\r");

        return EXIT_FAILURE;
    }

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= MEDIUM_VERBOSITY
    for (i = 0; i < M_CONSTANT; i++)
    {
        unsigned int j;

        printf("SIGMA[ %02u ] =  [%u", i, sigma[i][0]);

        for (j = 1; j < N_CONSTANT; j++)
        {
            printf(", %u", sigma[i][j]);
        }

        printf("]\n\r");
    }
#endif

    // Verify signature
    if (sots_core_verify_signature(params,
                                   sigma, sizeof(sigma) / sizeof(sigma[0]),
                                   pk, pk_len) != EXIT_SUCCESS)
    {
        printf("ERROR in KAT:  Signature is NOT valid! Exiting...\n\r");

        return EXIT_FAILURE;
    }

    // Compare result
    if (memcmp(sigma, KAT_SIGMA_CONSTANTS, sizeof(sigma)) != 0)
    {
        printf("ERROR in KAT:  Signature does NOT match expected result. Exiting...\n\r");

        return EXIT_FAILURE;
    }

    sots_cleanup(params);

    EndTestTimer(timeDiff_usec, testEarlier);

    printf("Known Answer Test timing:  %f seconds; %f msec; %f usec\n\r",
           timeDiff_usec * USEC_TO_SEC,
           timeDiff_usec * USEC_TO_MSEC,
           timeDiff_usec);

    printf("\n\rKnown Answer Test completed successfully!\n\r\n\r");
    printf("##################################################################################################\n\r\n\r");

    return EXIT_SUCCESS;
}

int perform_SOTS_SignatureTest(sots_params *params,
                               BitSequence pk[E_CONSTANT][N_CONSTANT], size_t pk_len,
                               BitSequence sk[E_CONSTANT][M_CONSTANT][N_CONSTANT], size_t sk_len,
                               bool useDeterministicData)
{
    uint64_t mainEarlier;
    int      signatureIndex;

    printf("Output using Envieta multiple precision code:\n\r");

    printf("\n\rTesting %d signatures..\n\r", SOTS_SIGNATURES);

    StartTestTimer(mainEarlier);
    for (signatureIndex = 0; signatureIndex < SOTS_SIGNATURES; signatureIndex++)
    {
        if (performSingleSignatureTest(params,
                                       pk, pk_len,
                                       sk, sk_len,
                                       useDeterministicData) != EXIT_SUCCESS)
        {
            printf("ERROR:  Detected invalid signature. Exiting...\n\r");

            return EXIT_FAILURE;
        }
    }
    EndTestTimer(timeDiff_usec, mainEarlier);

    printTimeReport(timeDiff_usec);

    sots_cleanup(params);

    printf("\n\rTest Complete!\n\r");

    return EXIT_SUCCESS;
}
