#include "multiprecision.h"

#include <assert.h>   // assert
#include <stdio.h>    // printf
#include <string.h>   // memset

#define BYTES_PER_DIGIT    (BITS_PER_DIGIT / 8)

/* Added in [v2.4] for ALLOC_BYTES and FREE_BYTES */
uint8_t zeroise_bytes(volatile void *v, size_t n)
{   /* Zeroise byte array b and make sure optimiser does not ignore this */
    volatile uint8_t  optdummy;
    volatile uint8_t *b = (uint8_t *)v;

    while (n--)
    {
        b[n] = 0;
    }
    optdummy = *b;

    return optdummy;
}

#define MAX_ALLOC_SIZE    (MAX_FIXED_DIGITS * BYTES_PER_DIGIT)
#define ALLOC_BYTES(b, n)    do { assert((n) <= sizeof((b))); zeroise_bytes((b), (n)); } while (0)
#define FREE_BYTES(b, n)     zeroise_bytes((b), (n))

__inline unsigned int mpAdd(DIGIT_T *c, const DIGIT_T *a, const DIGIT_T *b, unsigned int nwords)
{   // Multiprecision addition, c = a+b, where lng(a) = lng(b) = nwords. Returns the carry bit.
    unsigned int i, carry = 0;

    for (i = 0; i < nwords; i++)
    {
        ADDC(carry, a[i], b[i], carry, c[i]);
    }

    return carry;
}

__inline unsigned int mpSubtract(DIGIT_T *c, const DIGIT_T *a, const DIGIT_T *b, unsigned int nwords)
{   // Multiprecision subtraction, c = a-b, where lng(a) = lng(b) = nwords. Returns the borrow bit.
    unsigned int i, borrow = 0;

    for (i = 0; i < nwords; i++)
    {
        SUBC(borrow, a[i], b[i], borrow, c[i]);
    }

    return borrow;
}

void digit_x_digit(DIGIT_T a, DIGIT_T b, DIGIT_T *c)
{   // Digit multiplication, digit * digit -> 2-digit result
    register DIGIT_T al, ah, bl, bh, temp;
    DIGIT_T          albl, albh, ahbl, ahbh, res1, res2, res3, carry;
    DIGIT_T          mask_low = (DIGIT_T)(-1) >> (sizeof(DIGIT_T) * 4), mask_high = (DIGIT_T)(-1) << (sizeof(DIGIT_T) * 4);

    al    = a & mask_low;                     // Low part
    ah    = a >> (sizeof(DIGIT_T) * 4);       // High part
    bl    = b & mask_low;
    bh    = b >> (sizeof(DIGIT_T) * 4);

    albl  = al * bl;
    albh  = al * bh;
    ahbl  = ah * bl;
    ahbh  = ah * bh;
    c[0]  = albl & mask_low;                  // C00

    res1  = albl >> (sizeof(DIGIT_T) * 4);
    res2  = ahbl & mask_low;
    res3  = albh & mask_low;
    temp  = res1 + res2 + res3;
    carry = temp >> (sizeof(DIGIT_T) * 4);
    c[0] ^= temp << (sizeof(DIGIT_T) * 4);    // C01

    res1  = ahbl >> (sizeof(DIGIT_T) * 4);
    res2  = albh >> (sizeof(DIGIT_T) * 4);
    res3  = ahbh & mask_low;
    temp  = res1 + res2 + res3 + carry;
    c[1]  = temp & mask_low;                  // C10
    carry = temp & mask_high;
    c[1] ^= (ahbh & mask_high) + carry;       // C11
}

void mpMultiply(DIGIT_T *c, const DIGIT_T *a, const DIGIT_T *b, unsigned int nwords)
{   // Multiprecision comba multiply, c = a*b, where lng(a) = lng(b) = nwords.
    unsigned int i, j;
    DIGIT_T      t = 0, u = 0, v = 0, UV[2];
    unsigned int carry = 0;

    for (i = 0; i < nwords; i++)
    {
        for (j = 0; j <= i; j++)
        {
            MUL(a[j], b[i - j], UV + 1, UV[0]);
            ADDC(0, UV[0], v, carry, v);
            ADDC(carry, UV[1], u, carry, u);
            t += carry;
        }
        c[i] = v;
        v    = u;
        u    = t;
        t    = 0;
    }

    for (i = nwords; i < 2 * nwords - 1; i++)
    {
        for (j = i - nwords + 1; j < nwords; j++)
        {
            MUL(a[j], b[i - j], UV + 1, UV[0]);
            ADDC(0, UV[0], v, carry, v);
            ADDC(carry, UV[1], u, carry, u);
            t += carry;
        }
        c[i] = v;
        v    = u;
        u    = t;
        t    = 0;
    }
    c[2 * nwords - 1] = v;
}

static DIGIT_T mpMultSub(DIGIT_T wn, DIGIT_T w[], const DIGIT_T v[],
                         DIGIT_T q, size_t n)
{   /*  Compute w = w - qv
     *  where w = (WnW[n-1]...W[0])
     *  return modified Wn.
     */
    DIGIT_T k, t[2];
    size_t  i;

    if (q == 0) /* No change */
    {
        return wn;
    }

    k = 0;

    for (i = 0; i < n; i++)
    {
        spMultiply(t, q, v[i]);
        w[i] -= k;
        if (w[i] > MAX_DIGIT - k)
        {
            k = 1;
        }
        else
        {
            k = 0;
        }
        w[i] -= t[0];
        if (w[i] > MAX_DIGIT - t[0])
        {
            k++;
        }
        k    += t[1];
    }

    /* Cope with Wn not stored in array w[0..n-1] */
    wn -= k;

    return wn;
}

static int QhatTooBig(DIGIT_T qhat, DIGIT_T rhat,
                      DIGIT_T vn2, DIGIT_T ujn2)
{   /*  Returns true if Qhat is too big
     *  i.e. if (Qhat * Vn-2) > (b.Rhat + Uj+n-2)
     */
    DIGIT_T t[2];

    spMultiply(t, qhat, vn2);
    if (t[1] < rhat)
    {
        return 0;
    }
    else if (t[1] > rhat)
    {
        return 1;
    }
    else if (t[0] > ujn2)
    {
        return 1;
    }

    return 0;
}

int mpDivide(DIGIT_T q[], DIGIT_T r[], const DIGIT_T u[], size_t udigits, DIGIT_T v[], size_t vdigits)
{   /*  Computes quotient q = u / v and remainder r = u mod v
     *  where q, r, u are multiple precision digits
     *  all of udigits and the divisor v is vdigits.
     *
     *  Ref: Knuth Vol 2 Ch 4.3.1 p 272 Algorithm D.
     *
     *  Do without extra storage space, i.e. use r[] for
     *  normalised u[], unnormalise v[] at end, and cope with
     *  extra digit Uj+n added to u after normalisation.
     *
     *  WARNING: this trashes q and r first, so cannot do
     *  u = u / v or v = u mod v.
     *  It also changes v temporarily so cannot make it const.
     */
    size_t   shift, n;
    int      m, j;
    DIGIT_T  bitmask, overflow;
    DIGIT_T  qhat, rhat, t[2];
    DIGIT_T *uu;

    /* Clear q and r */
    mpSetZero(q, (const unsigned int)udigits);
    mpSetZero(r, (const unsigned int)udigits);

    /* Work out exact sizes of u and v */
    n  = mpSizeof(v, vdigits);
    m  = (int)mpSizeof(u, udigits);
    m -= (int)n;

    /* Catch special cases */
    if (n == 0)
    {
        return -1;  /* Error: divide by zero */
    }
    if (n == 1)
    {   /* Use short division instead */
        r[0] = mpShortDiv(q, u, v[0], udigits);

        return 0;
    }

    if (m < 0)
    {   /* v > u, so just set q = 0 and r = u */
        mpSetEqual(r, u, udigits);

        return 0;
    }

    if (m == 0)
    {   /* u and v are the same length */
        int cmp = mpCompare(u, v, n);

        if (cmp < 0)
        {   /* v > u, as above */
            mpSetEqual(r, u, udigits);

            return 0;
        }
        else if (cmp == 0)
        {   /* v == u, so set q = 1 and r = 0 */
            mpSetDigit(q, 1, (const unsigned int)udigits);

            return 0;
        }
    }

    /*  In Knuth notation, we have:
     *  Given
     *  u = (Um+n-1 ... U1U0)
     *  v = (Vn-1 ... V1V0)
     *  Compute
     *  q = u/v = (QmQm-1 ... Q0)
     *  r = u mod v = (Rn-1 ... R1R0)
     */

    /*  Step D1. Normalise */
    /*  Requires high bit of Vn-1
     *  to be set, so find most signif. bit then shift left,
     *  i.e. d = 2^shift, u' = u * d, v' = v * d.
     */
    bitmask = HIBITMASK;
    for (shift = 0; shift < BITS_PER_DIGIT; shift++)
    {
        if (v[n - 1] & bitmask)
        {
            break;
        }
        bitmask >>= 1;
    }

    /* Normalise v in situ - NB only shift non-zero digits */
    mpShiftLeft(v, v, shift, n);

    /* Copy normalised dividend u*d into r */
    overflow = mpShiftLeft(r, u, shift, n + (size_t)m);
    uu       = r;        /* Use ptr to keep notation constant */

    t[0]     = overflow; /* Extra digit Um+n */

    /* Step D2. Initialise j. Set j = m */
    for (j = m; j >= 0; j--)
    {
        DIGIT_T *ww;
        int      qhatOK;

        /* Step D3. Set Qhat = [(b.Uj+n + Uj+n-1)/Vn-1]
         * and Rhat = remainder */
        qhatOK   = 0;
        t[1]     = t[0]; /* This is Uj+n */
        t[0]     = uu[(unsigned long)j + n - 1u];
        overflow = spDivide(&qhat, &rhat, t, v[n - 1]);

        /* Test Qhat */
        if (overflow)
        {   /* Qhat == b so set Qhat = b - 1 */
            qhat  = MAX_DIGIT;
            rhat  = uu[(unsigned long)j + n - 1u];
            rhat += v[n - 1];
            if (rhat < v[n - 1])  /* Rhat >= b, so no re-test */
            {
                qhatOK = 1;
            }
        }
        /* [VERSION 2: Added extra test "qhat && "] */
        if (qhat && !qhatOK && QhatTooBig(qhat, rhat, v[n - 2], uu[(unsigned long)j + n - 2]))
        {   /* If Qhat.Vn-2 > b.Rhat + Uj+n-2
             * decrease Qhat by one, increase Rhat by Vn-1
             */
            qhat--;
            rhat += v[n - 1];
            /* Repeat this test if Rhat < b */
            if (rhat >= v[n - 1u])
            {
                if (QhatTooBig(qhat, rhat, v[n - 2], uu[(unsigned long)j + n - 2]))
                {
                    qhat--;
                }
            }
        }


        /* Step D4. Multiply and subtract */
        ww       = &uu[j];
        overflow = mpMultSub(t[1], ww, v, qhat, n);

        /* Step D5. Test remainder. Set Qj = Qhat */
        q[j]     = qhat;
        if (overflow)
        {   /* Step D6. Add back if D4 was negative */
            q[j]--;
            mpAdd(ww, ww, v, (const unsigned int)n);
        }

        t[0] = uu[(unsigned long)j + n - 1u]; /* Uj+n on next round */
    }                                         /* Step D7. Loop on j */

    /* Clear high digits in uu */
    for (j = (int)n; j < m + (int)n; j++)
    {
        uu[j] = 0;
    }

    /* Step D8. Unnormalise. */

    mpShiftRight(r, r, shift, n);
    mpShiftRight(v, v, shift, n);

    return 0;
}

__inline void mpSetZero(DIGIT_T *a, unsigned int nwords)
{   // Zero a field element, a = 0.
    unsigned int i;

    for (i = 0; i < nwords; i++)
    {
        a[i] = 0;
    }
}

__inline void mpSetDigit(DIGIT_T *a, DIGIT_T d, unsigned int nwords)
{
    unsigned int i;

    for (i = 1; i < nwords; i++)
    {
        a[i] = 0;
    }
    a[0] = d;
}

void mpShiftLeftInPlace(DIGIT_T *x, unsigned int shift, unsigned int nwords)
{
    unsigned int i, j = 0;

    while (shift > RADIX)
    {
        j     += 1;
        shift -= RADIX;
    }

    for (i = 0; i < nwords - j; i++)
    {
        x[nwords - 1 - i] = x[nwords - 1 - i - j];
    }
    for (i = nwords - j; i < nwords; i++)
    {
        x[nwords - 1 - i] = 0;
    }
    if (shift != 0)
    {
        for (j = nwords - 1; j > 0; j--)
        {
            SHIFTL(x[j], x[j - 1], shift, x[j], RADIX);
        }
        x[0] <<= shift;
    }
}

void mpOrBits(DIGIT_T a[], const DIGIT_T b[], const DIGIT_T c[], size_t nwords)
/* Computes bitwise a = b OR c */
{
    size_t i;

    for (i = 0; i < nwords; i++)
    {
        a[i] = b[i] | c[i];
    }
}

__inline void mpSetEqual(DIGIT_T a[], const DIGIT_T b[], size_t nwords)
{   /* Sets a = b */
    size_t i;

    for (i = 0; i < nwords; i++)
    {
        a[i] = b[i];
    }
}

/* Returns sign of (a - b) as 0, +1 or -1. Not constant-time. */
__inline int mpCompare(const DIGIT_T a[], const DIGIT_T b[], size_t nwords)
{
    /* if (nwords == 0) return 0; // deleted [v2.5] */

    while (nwords--)
    {
        if (a[nwords] > b[nwords])
        {
            return 1;   /* GT */
        }
        if (a[nwords] < b[nwords])
        {
            return -1;  /* LT */
        }
    }

    return 0;   /* EQ */
}

/** Returns true if a == 0, else false. Not constant-time. */
int mpIsZero(const DIGIT_T a[], size_t nwords)
{
    size_t i;

    /* if (nwords == 0) return -1; // deleted [v2.5] */

    for (i = 0; i < nwords; i++)   /* Start at lsb */
    {
        if (a[i] != 0)
        {
            return 0;   /* False */
        }
    }

    return 1;      /* True */
}

//int mpModInv(DIGIT_T inv[], const DIGIT_T u[], const DIGIT_T v[], size_t nwords)
//{   /*  Computes inv = u^(-1) mod v */
//    /*  Ref: Knuth Algorithm X Vol 2 p 342
//     *  ignoring u2, v2, t2
//     *  and avoiding negative numbers.
//     *  Returns non-zero if inverse undefined.
//     */
//    int      bIterations;
//    int      result;
//
///* Allocate temp storage */
//    DIGIT_T  u1[MAX_FIXED_DIGITS];
//    DIGIT_T  u3[MAX_FIXED_DIGITS];
//    DIGIT_T  v1[MAX_FIXED_DIGITS];
//    DIGIT_T  v3[MAX_FIXED_DIGITS];
//    DIGIT_T  t1[MAX_FIXED_DIGITS];
//    DIGIT_T  t3[MAX_FIXED_DIGITS];
//    DIGIT_T  q[MAX_FIXED_DIGITS];
//    DIGIT_T  w[2 * MAX_FIXED_DIGITS];
//    assert(nwords <= MAX_FIXED_DIGITS);
//
//    /* Step X1. Initialise */
//    mpSetDigit(u1, 1, nwords);    /* u1 = 1 */
//    mpSetEqual(u3, u, nwords);    /* u3 = u */
//    mpSetZero(v1, nwords);        /* v1 = 0 */
//    mpSetEqual(v3, v, nwords);    /* v3 = v */
//
//    bIterations = 1;               /* Remember odd/even iterations */
//    while (!mpIsZero(v3, nwords)) /* Step X2. Loop while v3 != 0 */
//    {                              /* Step X3. Divide and "Subtract" */
//        mpDivide(q, t3, u3, nwords, v3, nwords);
//        /* q = u3 / v3, t3 = u3 % v3 */
//        mpMultiply(w, q, v1, nwords);  /* w = q * v1 */
//        mpAdd(t1, u1, w, nwords);      /* t1 = u1 + w */
//
//        /* Swap u1 = v1; v1 = t1; u3 = v3; v3 = t3 */
//        mpSetEqual(u1, v1, nwords);
//        mpSetEqual(v1, t1, nwords);
//        mpSetEqual(u3, v3, nwords);
//        mpSetEqual(v3, t3, nwords);
//
//        bIterations = -bIterations;
//    }
//
//    if (bIterations < 0)
//    {
//        mpSubtract(inv, v, u1, nwords);    /* inv = v - u1 */
//    }
//    else
//    {
//        mpSetEqual(inv, u1, nwords);   /* inv = u1 */
//    }
//    /* Make sure u3 = gcd(u,v) == 1 */
//    if (mpShortCmp(u3, 1, nwords) != 0)
//    {
//        result = 1;
//        mpSetZero(inv, nwords);
//    }
//    else
//    {
//        result = 0;
//    }
//
//    return result;
//}

/** Returns number of significant digits in a */
size_t mpSizeof(const DIGIT_T a[], size_t nwords)
{
    while (nwords--)
    {
        if (a[nwords] != 0)
        {
            return ++nwords;
        }
    }

    return 0;
}

size_t mpBitLength(const DIGIT_T d[], size_t ndigits)
/* Returns no of significant bits in d */
{
    size_t  n, i, bits;
    DIGIT_T mask;

    if (!d || ndigits == 0)
    {
        return 0;
    }

    n = mpSizeof(d, ndigits);
    if (0 == n)
    {
        return 0;
    }

    for (i = 0, mask = HIBITMASK; mask > 0; mask >>= 1, i++)
    {
        if (d[n - 1] & mask)
        {
            break;
        }
    }

    bits = n * BITS_PER_DIGIT - i;

    return bits;
}

DIGIT_T mpShortDiv(DIGIT_T q[], const DIGIT_T u[], DIGIT_T v, size_t nwords)
{
    /*  Calculates quotient q = u div v
     *  Returns remainder r = u mod v
     *  where q, u are multiprecision integers of nwords each
     *  and r, v are single precision digits.
     *
     *  Makes no assumptions about normalisation.
     *
     *  Ref: Knuth Vol 2 Ch 4.3.1 Exercise 16 p625
     */
    size_t  j;
    DIGIT_T t[2], r;
    size_t  shift;
    DIGIT_T bitmask, overflow, *uu;

    if (nwords == 0)
    {
        return 0;
    }
    if (v == 0)
    {
        return 0;           /* Divide by zero error */
    }
    /*  Normalise first */
    /*  Requires high bit of V
     *  to be set, so find most signif. bit then shift left,
     *  i.e. d = 2^shift, u' = u * d, v' = v * d.
     */
    bitmask = HIBITMASK;
    for (shift = 0; shift < BITS_PER_DIGIT; shift++)
    {
        if (v & bitmask)
        {
            break;
        }
        bitmask >>= 1;
    }

    v      <<= shift;
    overflow = mpShiftLeft(q, u, shift, nwords);
    uu       = q;

    /* Step S1 - modified for extra digit. */
    r        = overflow; /* New digit Un */
    j        = nwords;
    while (j--)
    {
        /* Step S2. */
        t[1] = r;
        t[0] = uu[j];
        spDivide(&q[j], &r, t, v);
    }

    /* Unnormalise */
    r >>= shift;

    return r;
}

int spMultiply(DIGIT_T p[2], DIGIT_T x, DIGIT_T y)
{
    /* Use a 64-bit temp for product */
    uint64_t t = (uint64_t)x * (uint64_t)y;

    /* then split into two parts */
    p[1] = (DIGIT_T)(t >> 32u);
    p[0] = (DIGIT_T)(t & 0xFFFFFFFFu);

    return 0;
}

DIGIT_T spDivide(DIGIT_T *pq, DIGIT_T *pr, const DIGIT_T u[2], DIGIT_T v)
{
    uint64_t uu, q;

    uu  = (uint64_t)u[1] << 32u | (uint64_t)u[0];
    q   = uu / (uint64_t)v;
    //r = uu % (uint64_t)v;
    *pr = (DIGIT_T)(uu - q * v);
    *pq = (DIGIT_T)(q & 0xFFFFFFFFu);

    return (DIGIT_T)(q >> 32u);
}

/**********************/
/* BITWISE OPERATIONS */
/**********************/

/** Computes a = b << x */
DIGIT_T mpShiftLeft(DIGIT_T a[], const DIGIT_T *b, size_t shift, size_t nwords)
{   /* Computes a = b << shift */
    /* [v2.1] Modified to cope with shift > BITS_PERDIGIT */
    size_t  i, y, bits;
    DIGIT_T mask, carry;

    /* Do we shift whole digits? */
    if (shift >= BITS_PER_DIGIT)
    {
        size_t nw = shift / BITS_PER_DIGIT;
        i = nwords;
        while (i--)
        {
            if (i >= nw)
            {
                a[i] = b[i - nw];
            }
            else
            {
                a[i] = 0;
            }
        }
        /* Call again to shift bits inside digits */
        bits  = shift % BITS_PER_DIGIT;
        carry = b[nwords - nw] << bits;
        if (bits)
        {
            carry |= mpShiftLeft(a, a, bits, nwords);
        }

        return carry;
    }
    else
    {
        bits = shift;
    }

    /* Construct mask = high bits set */
    mask  = ~(~(DIGIT_T)0 >> bits);

    y     = BITS_PER_DIGIT - bits;
    carry = 0;
    for (i = 0; i < nwords; i++)
    {
        DIGIT_T nextcarry = (b[i] & mask) >> y;

        a[i]  = b[i] << bits | carry;
        carry = nextcarry;
    }

    return carry;
}

DIGIT_T mpShiftRight(DIGIT_T a[], const DIGIT_T b[], size_t shift, size_t ndigits)
{   /* Computes a = b >> shift */
    /* [v2.1] Modified to cope with shift > BITS_PERDIGIT */
    size_t  i, y, bits;
    DIGIT_T mask, carry;

    /* Do we shift whole digits? */
    if (shift >= BITS_PER_DIGIT)
    {
        size_t nw = shift / BITS_PER_DIGIT;
        for (i = 0; i < ndigits; i++)
        {
            if ((i + nw) < ndigits)
            {
                a[i] = b[i + nw];
            }
            else
            {
                a[i] = 0;
            }
        }
        /* Call again to shift bits inside digits */
        bits  = shift % BITS_PER_DIGIT;
        carry = b[nw - 1] >> bits;
        if (bits)
        {
            carry |= mpShiftRight(a, a, bits, ndigits);
        }

        return carry;
    }
    else
    {
        bits = shift;
    }

    /* Construct mask to set low bits */
    /* (thanks to Jesse Chisholm for suggesting this improved technique) */
    mask  = ~(~(DIGIT_T)0 << bits);

    y     = BITS_PER_DIGIT - bits;
    carry = 0;
    i     = ndigits;
    while (i--)
    {
        DIGIT_T nextcarry = (b[i] & mask) << y;

        a[i]  = b[i] >> bits | carry;
        carry = nextcarry;
    }

    return carry;
}

/*******************/
/* PRINT UTILITIES */
/*******************/

static size_t uiceil(double x)
/* Returns ceil(x) as a non-negative integer or 0 if x < 0 */
{
    size_t c;

    if (x < 0)
    {
        return 0;
    }
    c = (size_t)x;
    if ((x - c) > 0.0)
    {
        c++;
    }

    return c;
}

size_t mpConvToOctets(const DIGIT_T a[], size_t ndigits, unsigned char *c, size_t nbytes)
/* Convert big digit a into string of octets, in big-endian order,
 * padding on the left to nbytes or truncating if necessary.
 * Return number of octets required excluding leading zero bytes.
 */
{
    int          j, len;
    unsigned int k;
    size_t       i, noctets, nbits;

    nbits   = mpBitLength(a, ndigits);
    noctets = (nbits + 7) / 8;

    len     = (int)nbytes;

    for (i = 0, j = len - 1; i < ndigits && j >= 0; i++)
    {
        DIGIT_T t = a[i];

        for (k = 0; j >= 0 && k < BITS_PER_DIGIT; j--, k += 8)
        {
            c[j] = (unsigned char)(t >> k);
        }
    }

    for (; j >= 0; j--)
    {
        c[j] = 0;
    }

    return (size_t)noctets;
}

static size_t conv_to_base(const DIGIT_T a[], size_t ndigits, char *s, size_t smax, int base)
/* Convert big digit a into a string in given base format,
 * where s has max size smax.
 * Return number of chars set excluding leading zeroes.
 * smax can be 0 to find out the required length.
 */
{
    uint8_t     bytes[MAX_ALLOC_SIZE], newdigits[MAX_ALLOC_SIZE * 3];   // [v2.6] increased
    const char  DEC_DIGITS[] = "0123456789";
    const char  HEX_DIGITS[] = "0123456789abcdef";
    size_t      newlen, nbytes, nchars;
    size_t      n;
    size_t      i, j, isig;
    const char *digits;
    double      factor;

    switch (base)
    {
    case 10:
        digits = DEC_DIGITS;
        factor = 2.40824;   /* log(256)/log(10)=2.40824 */
        break;

    case 16:
        digits = HEX_DIGITS;
        factor = 2.0;   /* log(256)/log(16)=2.0 */
        break;

    default:
        assert(10 == base || 16 == base);

        return 0;
    }

    /* Set up output string with null chars */
    if (smax > 0 && s)
    {
        memset(s, '0', smax - 1);
        s[smax - 1] = '\0';
    }

    /* Catch zero input value (return 1 not zero) */
    if (mpIsZero(a, ndigits))
    {
        if (smax > 0 && s)
        {
            s[1] = '\0';
        }

        return 1;
    }

    /* First, we convert to 8-bit octets (bytes), which are easier to handle */
    nbytes = ndigits * BITS_PER_DIGIT / 8;
    ALLOC_BYTES(bytes, nbytes);

    n      = mpConvToOctets(a, ndigits, bytes, nbytes);

    /* Create some temp storage for int values */
    newlen = uiceil(n * factor);
    ALLOC_BYTES(newdigits, newlen);

    for (i = 0; i < nbytes; i++)
    {
        unsigned long t = bytes[i];

        for (j = newlen; j > 0; j--)
        {
            t               += (unsigned long)newdigits[j - 1] * 256;
            newdigits[j - 1] = (unsigned char)(t % (unsigned long)base);
            t               /= (unsigned long)base;
        }
    }

    /* Find index of leading significant digit */
    for (isig = 0; isig < newlen; isig++)
    {
        if (newdigits[isig])
        {
            break;
        }
    }

    nchars = newlen - isig;

    /* Convert to a null-terminated string of decimal chars */
    /* up to limit, unless user has specified null or size == 0 */
    if (smax > 0 && s)
    {
        for (i = 0; i < nchars && i < smax - 1; i++)
        {
            s[i] = digits[newdigits[isig + i]];
        }
        s[i] = '\0';
    }

    FREE_BYTES(bytes, nbytes);
    FREE_BYTES(newdigits, newlen);

    return nchars;
}

void mpPrintDecimal(const char *prefix, const DIGIT_T *a, size_t len, const char *suffix)
{
    char   s[MAX_ALLOC_SIZE * 3];
    size_t nc;

    /* Put big digit into a string of decimal chars */
    nc = mpConvToDecimal(a, len, NULL, 0);
    ALLOC_BYTES(s, nc + 1);
    mpConvToDecimal(a, len, s, nc + 1);
    if (prefix)
    {
        printf("%s", prefix);
    }

    printf("%s", s);

    if (suffix)
    {
        printf("%s", suffix);
    }
}

void mpPrintHex(const char *prefix, const DIGIT_T *a, size_t len, const char *suffix)
{
    if (prefix)
    {
        printf("%s", prefix);
    }
    /* Trim leading digits which are zero */
    while (len--)
    {
        if (a[len] != 0)
        {
            break;
        }
    }
    len++;
    if (0 == len)
    {
        len = 1;
    }
    /* print first digit without leading zeros */
    printf("%" PRIXBIGD, a[--len]);
    while (len--)
    {
#if (TARGET == TARGET_AMD64)
        printf("%016" PRIXBIGD, a[len]);
#else
        printf("%08" PRIXBIGD, a[len]);
#endif
    }
    if (suffix)
    {
        printf("%s", suffix);
    }
}

size_t mpConvToDecimal(const DIGIT_T a[], size_t ndigits, char *s, size_t smax)
/* Convert big digit a into a string in decimal format,
 * where s has max size smax.
 * Return number of chars set excluding leading zeroes.
 */
{
    return conv_to_base(a, ndigits, s, smax, 10);
}
