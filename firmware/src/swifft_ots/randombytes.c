/*
 * This code was taken from the SPHINCS reference implementation and is public domain.
 */

#include <fcntl.h>
#include "randombytes.h"

/* JMR Modified approach using AES Encryption */
void randombytes(unsigned char *x, unsigned long xlen)
{
    uint64_t      rand64 = 0;
    unsigned char bytes[8];
    int           i;
    int           j;

    while (xlen > 0)
    {
        rand64 = RANDOM64;

        ull_to_bytes(bytes, 8, rand64);
        if (xlen < 8)
        {
            i = xlen;
        }
        else
        {
            i = 8;
        }

        for (j = 0; j < i; ++j)
        {
            *(x + j) = bytes[j];
        }

        x    += i;
        xlen -= i;
    }
}
