#include "params.h"
#ifdef RANDOMIZE_M_DATA
#   include "randombytes.h"
#endif
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>     // memset, memcpy

int configure_SOTS_Params(sots_params *params, bool useDeterministicData)
{
    unsigned int  i;
    DIGIT_T       num1[NWORDS_FIELD];
    DIGIT_T       num2[NWORDS_FIELD];
    DIGIT_T       num3[NWORDS_FIELD];

    // Initialize m to static value at first; if RANDOMIZE_M_DATA is set, then this will be overwritten later
    // m = 0XBBA4307F2D136D04E75C8F550073304DD2F76225785B42EF256E96FBL
    unsigned char m[SOTS_MLEN] = { 0xBB, 0xA4, 0x30, 0x7F, 0x2D, 0x13, 0x6D, 0x04, 0xE7, 0x5C, 0x8F, 0x55, 0x00, 0x73, 0x30, 0x4D, 0xD2, 0xF7, 0x62, 0x25, 0x78, 0x5B, 0x42, 0xEF, 0x25, 0x6E, 0x96, 0xFB };

    // Clear out big digits
    mpSetZero(num1, NWORDS_FIELD);
    mpSetZero(num2, NWORDS_FIELD);
    mpSetZero(num3, NWORDS_FIELD);
    mpSetZero(params->eChooseK, NWORDS_FIELD);
    mpSetZero(params->m_bigInt, NWORDS_FIELD);
    for (i = 0; i < NUM_ROWS; i++)
    {
        mpSetZero(params->first_row[i], NWORDS_FIELD);
    }

    // params->m_bigInt = m[0]
    mpSetDigit(params->m_bigInt, m[0], NWORDS_FIELD);
    for (i = 1; i < SOTS_MLEN; i++)
    {
        // num1 = m[i]
        mpSetDigit(num1, m[i], NWORDS_FIELD);
        // params->m_bigInt = params->m_bigInt << 8
        mpShiftLeftInPlace(params->m_bigInt, 8, NWORDS_FIELD);
        // params->m_bigInt = params->m_bigInt | num1
        mpOrBits(params->m_bigInt, num1, params->m_bigInt, NWORDS_FIELD);
    }
#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= MEDIUM_VERBOSITY
    mpPrintHex("params->m_bigInt in base 16 = ", params->m_bigInt, NWORDS_FIELD, "\n\r");
#endif

    // Python:  OMEGA_inv = modinv(OMEGA, q)
#ifdef PERFORM_CONSTANTS_COMPUTATION
    // Compute modular inverse of Omega, i.e. inv = OMEGA_CONSTANT^(-1) mod q_CONSTANT
    params->OMEGA_inv = (BitSequence)modinv(OMEGA_CONSTANT, q_CONSTANT);
#else
    params->OMEGA_inv = OMEGA_INV_CONSTANT;
#endif
#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= MEDIUM_VERBOSITY
    printf("OMEGA_inv is %u\n\r", params->OMEGA_inv);
#endif

    // Python:  psi_rev = [(params->OMEGA ** bit_reverse(i)) % q for i in range(N)]
#ifdef PERFORM_CONSTANTS_COMPUTATION
    for (i = 0; i < N_CONSTANT; i++)
    {
        params->psi_rev[i] = (BitSequence)modular_power(OMEGA_CONSTANT, bit_reverse(i, 6u), q_CONSTANT);
#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= HIGH_VERBOSITY
        printf("params->psi_rev[%2u] = (%u ^ %2u) %% %i = %u\n\r",
               i,
               OMEGA_CONSTANT,
               bit_reverse(i, 6u),
               q_CONSTANT,
               params->psi_rev[i]);
#endif
    }
#else
    memcpy(params->psi_rev, &PSI_REV_CONSTANTS, sizeof(params->psi_rev));
#endif
#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= MEDIUM_VERBOSITY
    printf("params->psi_rev     = { %3u", params->psi_rev[0]);
    for (i = 1; i < N_CONSTANT; i++)
    {
        printf(", %3u", params->psi_rev[i]);
    }
    printf("}\n\r");
#endif

    // Python:  psi_inv_rev = [(params->OMEGA_inv ** bit_reverse(i)) % q for i in range(N)]
#ifdef PERFORM_CONSTANTS_COMPUTATION
    for (i = 0; i < N_CONSTANT; i++)
    {
        params->psi_inv_rev[i] = (BitSequence)modular_power(params->OMEGA_inv, bit_reverse(i, 6u), q_CONSTANT);
#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= HIGH_VERBOSITY
        printf("params->psi_inv_rev[%2u] = (%u ^ %2u) %% %d = %u\n\r",
               i,
               params->OMEGA_inv,
               bit_reverse(i, 6u),
               q_CONSTANT,
               params->psi_inv_rev[i]);
#endif
    }
#else
    memcpy(params->psi_inv_rev, &PSI_INV_REV_CONSTANTS, sizeof(params->psi_inv_rev));
#endif
#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= MEDIUM_VERBOSITY
    printf("params->psi_inv_rev = { %3u", params->psi_inv_rev[0]);
    for (i = 1; i < N_CONSTANT; i++)
    {
        printf(", %3u", params->psi_inv_rev[i]);
    }
    printf("}\n\r");
#endif

    // Python:  first_row = [comb(E - i, K - 1, exact = 1) for i in range(1, E - K + 3)]
#if defined(PERFORM_CONSTANTS_COMPUTATION) && (TARGET == TARGET_x86)
    for (i = 0; i < NUM_ROWS; i++)
    {
        if (mp_combi(params->first_row[i], E_CONSTANT - i - 1u, K_CONSTANT - 1u) != EXIT_SUCCESS)
        {
            printf("Error performing mp_combi(%d, %d).\n\r",
                   E_CONSTANT - i - 1,
                   K_CONSTANT - 1);

            return EXIT_FAILURE;
        }
    }
#else
    memcpy(params->first_row, &FIRST_ROW_CONSTANTS, sizeof(params->first_row));
#endif
#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= MEDIUM_VERBOSITY
    printf("\n\rparams->first_row:\n\r");
    for (i = 0; i < NUM_ROWS; i++)
    {
        printf("\tparams->first_row[%03u] ", i);
        mpPrintDecimal("= ", params->first_row[i], NWORDS_FIELD, "\n\r");
    }
#endif

    // Python:  echoosek = comb(params->E, params->K, exact = 1)
#if defined(PERFORM_CONSTANTS_COMPUTATION) && (TARGET == TARGET_x86)
    if (mp_combi(params->eChooseK, E_CONSTANT, K_CONSTANT) != EXIT_SUCCESS)
    {
        printf("Error performing mp_combi(%d, %d).\n\r",
               E_CONSTANT,
               K_CONSTANT);

        return EXIT_FAILURE;
    }
#else
    memcpy(params->eChooseK, &E_CHOOSE_K_CONSTANT, sizeof(params->eChooseK));
#endif
#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= MEDIUM_VERBOSITY
    mpPrintDecimal("\tparams->eChooseK = ", params->eChooseK, NWORDS_FIELD, "\n\r");
#endif

#ifdef RANDOMIZE_A_DATA
    printf("Note that the A parameter will be randomized before each keypair is generated\n\r");
#else
    printf("Note that the A parameter will be held constant across all keypair generation\n\r");
#endif

    // Initialize A
    if (useDeterministicData)
    {
        // Initialize A to static values for testing
        memcpy(params->A, &A_DEFAULT, sizeof(params->A));
    }
    else
    {
        randomizeA(params);
    }

    return EXIT_SUCCESS;
}

void sots_cleanup(sots_params *params)
{
    unsigned int i;

    // Zero out big nums
    for (i = 0; i < NUM_ROWS; i++)
    {
        mpSetZero(params->first_row[i], NWORDS_FIELD);
    }
    mpSetZero(params->eChooseK, NWORDS_FIELD);
    mpSetZero(params->m_bigInt, NWORDS_FIELD);
}

void randomizeA(sots_params *params)
{
    unsigned int i, j;

    // Note that srand() should be called just once in the main() function
    for (i = 0; i < M_CONSTANT; i++)
    {
        for (j = 0; j < N_CONSTANT; j++)
        {
            params->A[i][j] = (BitSequence)(rand() % q_CONSTANT);
        }
    }
}

#ifdef RANDOMIZE_M_DATA
void randomizeM(sots_params *params)
{
    unsigned int  i;
    DIGIT_T       num1[NWORDS_FIELD];
    unsigned char m[SOTS_MLEN];

    // Zero out Big Num
    mpSetZero(num1, NWORDS_FIELD);

    randombytes(m, SOTS_MLEN);

    // params->m_bigInt = m[0]
    mpSetDigit(params->m_bigInt, m[0], NWORDS_FIELD);
    for (i = 1; i < SOTS_MLEN; i++)
    {
        // num1 = m[i]
        mpSetDigit(num1, m[i], NWORDS_FIELD);
        // params->m_bigInt = params->m_bigInt << 8
        mpShiftLeftInPlace(params->m_bigInt, 8, NWORDS_FIELD);
        // params->m_bigInt = params->m_bigInt | num1
        mpOrBits(params->m_bigInt, num1, params->m_bigInt, NWORDS_FIELD);
    }
#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= MEDIUM_VERBOSITY
    mpPrintDecimal("params->m_bigInt in base 10 = ", params->m_bigInt, NWORDS_FIELD, "\n\r");
    mpPrintHex("params->m_bigInt in base 16 = ", params->m_bigInt, NWORDS_FIELD, "\n\r");
#endif
}
#endif
