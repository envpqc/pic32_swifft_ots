#ifndef _SOTS_UTILS_H_
#define _SOTS_UTILS_H_

#include "params.h"

#include <stdbool.h>    // bool
#include <sys/time.h>   // timeval

/**
 * Converts the value of 'in' to 'outlen' bytes in big-endian byte order.
 */
void ull_to_bytes(unsigned char *out, unsigned int outlen,
                  unsigned long long in);

void extendedEuclid(long *result, long a, long b);

long modinv(long a, long m);

// Calculates bit reversal permutation.
//
// Parameters:
// - input: the input to reverse.
// - numOfBits: the number of bits in the input to reverse.
//
// Returns:
// - The resulting number, which is obtained from the input by reversing its bits.
unsigned int bit_reverse(unsigned int input, unsigned int numOfBits);

#if defined(PERFORM_CONSTANTS_COMPUTATION) && (TARGET == TARGET_x86)
/**
 * result = C(n, r)
 */
int mp_combi(DIGIT_T result[], unsigned int n, unsigned int r);
#endif

unsigned long power(unsigned long base, unsigned long exp);

unsigned long modular_power(unsigned long base, unsigned long exp, unsigned long modulus);

#endif /* _SOTS_UTILS_H_ */
