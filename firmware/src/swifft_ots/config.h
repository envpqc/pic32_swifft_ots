/********************************************************************************************
 * SWIFFT One Time Signature Libary
 *
 * Abstract: configuration file and platform-dependent macros
 *********************************************************************************************/

#ifndef _SOTS_CONFIG_H_
#define _SOTS_CONFIG_H_

#include <stdint.h>     // uint32_t

// Definition of compiler
#define COMPILER_GCC      1
#define COMPILER_CLANG    2

#if defined(__GNUC__)           // GNU GCC compiler
    #define COMPILER      COMPILER_GCC
#elif defined(__clang__)        // Clang compiler
    #define COMPILER      COMPILER_CLANG
#else
    #error -- "Unsupported COMPILER"
#endif

#define NO_VERBOSITY          0
#define LOW_VERBOSITY         1
#define MEDIUM_VERBOSITY      2
#define HIGH_VERBOSITY        3
#define ULTRA_VERBOSITY       4
#define VERBOSITY_LEVEL       NO_VERBOSITY

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= HIGH_VERBOSITY
#   define SOTS_SIGNATURES    1
#elif defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= LOW_VERBOSITY
#   define SOTS_SIGNATURES    10
#else
#   define SOTS_SIGNATURES    1000
#endif

/* Only use deterministic testing data for High Verbosity case, i.e. one signature */
#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= HIGH_VERBOSITY
#   define USE_DETERMINISTIC_DATA
#endif

// Turn on random m for all cases lower than High Verbosity case, i.e. just one signature
#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL < HIGH_VERBOSITY
#   define RANDOMIZE_M_DATA
#endif

/*
 * Uncomment the below #define to compute the constants vice using predefined values
 */
//#define PERFORM_CONSTANTS_COMPUTATION

/*
 * Uncomment the below #define to validate the dimensions of arrays
 */
//#define PERFORM_ARRAY_DIMENSION_VALIDATION

/*
 * Uncomment the below #define to randomize A every time the keypair is generated
 */
#define RANDOMIZE_A_DATA

/*
 * Uncomment the below #define to regenerate the keypair for every new signature
 */
#define REGENERATE_KEYPAIR


// Definition of the targeted architecture and basic data types

#define TARGET_AMD64     1
#define TARGET_x86       2

#define USE_64_BIT_PROCESSING

#ifdef USE_64_BIT_PROCESSING
#define TARGET       TARGET_AMD64
#define RADIX        64u
#define LOG2RADIX    6u
typedef uint64_t     DIGIT_T;                 // Unsigned 64-bit digit
#else
#define TARGET       TARGET_x86
#define RADIX        32u
#define LOG2RADIX    5u
typedef uint32_t     DIGIT_T;                 // Unsigned 32-bit digit
#endif


/********************** Constant-time unsigned comparisons ***********************/

// The following functions return 1 (TRUE) if condition is true, 0 (FALSE) otherwise

static __inline unsigned int is_digit_nonzero_ct(DIGIT_T x)
{   // Is x != 0?
    return (unsigned int)((x | (0u - x)) >> (RADIX - 1u));
}

static __inline unsigned int is_digit_zero_ct(DIGIT_T x)
{   // Is x = 0?
    return 1u ^ is_digit_nonzero_ct(x);
}

static __inline unsigned int is_digit_lessthan_ct(DIGIT_T x, DIGIT_T y)
{   // Is x < y?
    return (unsigned int)((x ^ ((x ^ y) | ((x - y) ^ y))) >> (RADIX - 1u));
}


/********************** Macros for platform-dependent operations **********************/

// Digit multiplication
#define MUL(multiplier, multiplicand, hi, lo) \
    digit_x_digit((multiplier), (multiplicand), &(lo));

// Digit addition with carry
#define ADDC(carryIn, addend1, addend2, carryOut, sumOut) \
    { DIGIT_T tempReg = (addend1) + (DIGIT_T)(carryIn);   \
      (sumOut)   = (addend2) + tempReg;                   \
      (carryOut) = (is_digit_lessthan_ct(tempReg, (DIGIT_T)(carryIn)) | is_digit_lessthan_ct((sumOut), tempReg)); }

// Digit subtraction with borrow
#define SUBC(borrowIn, minuend, subtrahend, borrowOut, differenceOut)                                                      \
    { DIGIT_T      tempReg   = (minuend) - (subtrahend);                                                                   \
      unsigned int borrowReg = (is_digit_lessthan_ct((minuend), (subtrahend)) | ((borrowIn) & is_digit_zero_ct(tempReg))); \
      (differenceOut) = tempReg - (DIGIT_T)(borrowIn);                                                                     \
      (borrowOut)     = borrowReg; }

// Shift left with flexible datatype
#define SHIFTL(highIn, lowIn, shift, shiftOut, DigitSize) \
    (shiftOut) = ((highIn) << (shift)) ^ ((lowIn) >> ((DigitSize) - (shift)));


#endif
