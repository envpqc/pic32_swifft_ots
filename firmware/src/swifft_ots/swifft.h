#ifndef _SOTS_SWIFFT_H_
#define _SOTS_SWIFFT_H_

#include "params.h"     // sots_params, BitSequence

/*
 * Uncomment the below define to switch to using SWIFFT_INTT vice SWIFFT_NTT
 */
//#define USE_SWIFFT_INTT_VICE_NTT

int new_enumerate(const sots_params *params, BitSequence *subsetE, size_t subsetE_len);

int SWIFFT(const sots_params *params,
           BitSequence x[M_CONSTANT][N_CONSTANT], size_t x_len,
           BitSequence returnVal[N_CONSTANT], size_t returnVal_len);

#ifdef USE_SWIFFT_INTT_VICE_NTT
int SWIFFT_INTT(const sots_params *params,
                BitSequence input[N_CONSTANT], size_t input_len);
#else
int SWIFFT_NTT(const sots_params *params,
               BitSequence input[N_CONSTANT], size_t input_len);
#endif

#endif
