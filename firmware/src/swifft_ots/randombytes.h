#ifndef _SOTS_RANDOMBYTES_H_
#define _SOTS_RANDOMBYTES_H_

#include "rand_openssl_aes.h"

/**
 * Tries to read xlen bytes from a source of randomness, and writes them to x.
 */
void randombytes(unsigned char *x, unsigned long xlen);

#endif
