#include "swifft.h"

#ifdef USE_SWIFFT_INTT_VICE_NTT
#include "utils.h"      // modinv
#endif

#include <stdio.h>      // printf
#include <stdlib.h>     // EXIT_SUCCESS, EXIT_FAILURE
#include <string.h>     // memset, memcpy


/*
 * Computes the SWIFFT hash function
 * Deep copy of x is made so that values are not overwritten by SWIFFT_NTT
 *
 * Python:
 * def SWIFFT(a, x):
 *     m = len(a)
 *     n = len(a[0])
 *     assert m == len(x)
 *     assert n == len(x[0])
 *     return_val = [0]*n
 *     x_copy = [[x[i][j] for j in range(n)] for i in range(m)]
 *     for i in range(m):
 *         SWIFFT_NTT(x_copy[i])
 *         return_val = [(return_val[j] + a[i][j] * x_copy[i][j])%q for j in range(n)]
 *     return return_val
 */
int SWIFFT(const sots_params *params,
           BitSequence x[M_CONSTANT][N_CONSTANT], size_t x_len,
           BitSequence returnVal[N_CONSTANT], size_t returnVal_len)
{
    int          result = EXIT_SUCCESS;
    unsigned int i, j;
    BitSequence  x_copy[M_CONSTANT][N_CONSTANT];

#ifdef PERFORM_ARRAY_DIMENSION_VALIDATION
    // Validate dimensions
    if (sizeof(params->A) / sizeof(params->A[0]) != M_CONSTANT)
    {
        printf("ERROR: sizeof(params->A) / sizeof(params->A[0]) not equal to expected size of %d; actual value = %u\n\r",
               M_CONSTANT,
               sizeof(params->A) / sizeof(params->A[0]));

        return EXIT_FAILURE;
    }
    if (x_len != M_CONSTANT)
    {
        printf("ERROR: x_len not equal to expected size of %d; actual value = %u\n\r",
               M_CONSTANT,
               x_len);

        return EXIT_FAILURE;
    }
    if (sizeof(params->A[0]) / sizeof(params->A[0][0]) != N_CONSTANT)
    {
        printf("ERROR: sizeof(a_len[0]) not equal to expected size of %d; actual value = %u\n\r",
               N_CONSTANT,
               sizeof(params->A[0]) / sizeof(params->A[0][0]));

        return EXIT_FAILURE;
    }
    if (sizeof(x[0]) / sizeof(x[0][0]) != N_CONSTANT)
    {
        printf("ERROR: sizeof(x[0]) not equal to expected size of %d; actual value = %u\n\r",
               N_CONSTANT,
               sizeof(x[0]));

        return EXIT_FAILURE;
    }
    if (returnVal_len != N_CONSTANT)
    {
        printf("ERROR: returnVal_len not equal to expected size of %d; actual value = %u\n\r",
               N_CONSTANT,
               returnVal_len);

        return EXIT_FAILURE;
    }
#endif

    memset(returnVal, 0x0, returnVal_len * sizeof(returnVal[0]));

    // Python:  x_copy = copy.deepcopy(x)
    memcpy(x_copy, x, x_len * sizeof(x[0]));

    for (i = 0; i < M_CONSTANT; i++)
    {
        // Python:  SWIFFT_NTT(x_copy[i])
#ifdef USE_SWIFFT_INTT_VICE_NTT
        SWIFFT_INTT(params, x_copy[i], sizeof(x_copy[0]) / sizeof(x_copy[0][0]));
#else
        SWIFFT_NTT(params, x_copy[i], sizeof(x_copy[0]) / sizeof(x_copy[0][0]));
#endif

        // Python:  return_val = [(return_val[j] + a[i][j] * x_copy[i][j]) % q for j in range(n)]
        for (j = 0; j < N_CONSTANT; j++)
        {
            returnVal[j] = (BitSequence)((returnVal[j] + params->A[i][j] * x_copy[i][j]) % q_CONSTANT);
        }
    }

    return result;
}

#ifdef USE_SWIFFT_INTT_VICE_NTT
/*
 * INTT from the Longa-Naehrig NTT paper
 * Computation is done in-place, no pre-mult by powers of psi needed
 * Output is the INTT in bit-reversed order
 *
 * Python:
 * def SWIFFT_INTT(input):
 *     n = len(input)
 *     k = 1
 *     m = n
 *     while m > 1:
 *         j1 = 0
 *         h = m / 2
 *         i = 0
 *         while i < h:
 *             j2 = j1 + k - 1
 *             S = psi_inv_rev[h+i]
 *             j = j1
 *             while j < j2 + 1:
 *                 U = input[j]
 *                 V = input[j+k]
 *                 input[j] = (U + V)%q
 *                 input[j+k] = ((U - V) * S)%q
 *                 j = j + 1
 *             i = i + 1
 *             j1 = j1 + 2*k
 *         m = m / 2
 *         k = 2 * k
 *     j = 0
 *     while j < n:
 *         input[j] = (input[j] * modinv(n,q))%q
 *         j = j + 1
 *     return input
 */
int SWIFFT_INTT(const sots_params *params,
                BitSequence input[N_CONSTANT], size_t input_len)
{
    int          result = EXIT_SUCCESS;
    unsigned int n, k, m, j2, S, j, U, V;

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= ULTRA_VERBOSITY
    unsigned int i;
#endif

#ifdef PERFORM_ARRAY_DIMENSION_VALIDATION
    // Validate dimensions
    if (input_len != N_CONSTANT)
    {
        printf("ERROR: input_len not equal to expected size of %d; actual value = %u\n\r",
               N_CONSTANT,
               input_len);

        return EXIT_FAILURE;
    }
#endif

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= ULTRA_VERBOSITY
    printf("SWIFFT_INTT input before work:  ");
    for (i = 0; i < N_CONSTANT; i++)
    {
        printf("%3u, ", input[i]);
    }
    printf("\n\r");
#endif

    n = (unsigned int)input_len;
    k = 1;
    m = n;
    while (m > 1)
    {
        unsigned int i  = 0;
        unsigned int h  = m / 2;
        unsigned int j1 = 0;

        while (i < h)
        {
            j2 = j1 + k - 1;
            S  = params->psi_inv_rev[h + i];
            j  = j1;

            while (j < (j2 + 1))
            {
                U            = input[j];
                V            = input[j + k];
                input[j]     = (BitSequence)((U + V) % q_CONSTANT);
                // JMR Note - have to do these extra steps to account for underflow and to match the way that modulus works in Python
                // See https://stackoverflow.com/questions/1907565/c-python-different-behaviour-of-the-modulo-operation for reference
                input[j + k] = (BitSequence)((((((int)U - (int)V) * (int)S) % q_CONSTANT) + q_CONSTANT) % q_CONSTANT);
                j            = j + 1;
            }
            i++;
            j1 = j1 + 2 * k;
        }
        m = m / 2;
        k = 2 * k;
    }
    j = 0;
    while (j < n)
    {
        input[j] = (BitSequence)((input[j] * modinv(n, q_CONSTANT)) % q_CONSTANT);
        j++;
    }

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= ULTRA_VERBOSITY
    printf("SWIFFT_INTT input  after work:  ");
    for (i = 0; i < N_CONSTANT; i++)
    {
        printf("%3u, ", input[i]);
    }
    printf("\n\r");
#endif

    return result;
}

#else

/*
 * NTT from the Longa-Naehrig NTT paper
 * Computation is done in-place, no pre-mult by powers of psi needed
 * Output is the NTT in bit-reversed order
 *
 * Python:
 * def SWIFFT_NTT(input):
 *     n = len(input)
 *     k = n
 *     m = 1
 *     while m < n:
 *         k = k / 2
 *         i = 0
 *         while i < m:
 *             j1 = 2 * i * k
 *             j2 = j1 + k - 1
 *             S = psi_rev[m+i]
 *             j = j1
 *             while j < j2 + 1:
 *                 U = input[j]
 *                 V = input[j+k]*S
 *                 input[j] = (U+V)%q
 *                 input[j+k] = (U-V)%q
 *                 j = j + 1
 *             i = i + 1
 *         m = 2 * m
 *     return input
 */
int SWIFFT_NTT(const sots_params *params,
               BitSequence input[N_CONSTANT], size_t input_len)
{
    int          result = EXIT_SUCCESS;
    unsigned int n, k, m, S, j, U, V;

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= ULTRA_VERBOSITY
    unsigned int i;
#endif

#ifdef PERFORM_ARRAY_DIMENSION_VALIDATION
    // Validate dimensions
    if (input_len != N_CONSTANT)
    {
        printf("ERROR: input_len not equal to expected size of %d; actual value = %u\n\r",
               N_CONSTANT,
               input_len);

        return EXIT_FAILURE;
    }
#endif

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= ULTRA_VERBOSITY
    printf("SWIFFT_NTT input before work:  ");
    for (i = 0; i < N_CONSTANT; i++)
    {
        printf("%3u, ", input[i]);
    }
    printf("\n\r");
#endif

    n = (unsigned int)input_len;
    k = n;
    m = 1;
    while (m < n)
    {
        unsigned int i = 0;
        k = k / 2;

        while (i < m)
        {
            unsigned int j1 = 2 * i * k;
            unsigned int j2 = j1 + k - 1;
            S = params->psi_rev[m + i];
            j = j1;

            while (j < (j2 + 1))
            {
                U            = input[j];
                V            = input[j + k] * S;
                input[j]     = (BitSequence)((U + V) % q_CONSTANT);
                // JMR Note - have to do these extra steps to account for underflow and to match the way that modulus works in Python
                // See https://stackoverflow.com/questions/1907565/c-python-different-behaviour-of-the-modulo-operation for reference
                input[j + k] = (BitSequence)(((((int)U - (int)V) % q_CONSTANT) + q_CONSTANT) % q_CONSTANT);
                j            = j + 1;
            }
            i = i + 1;
        }
        m = 2 * m;
    }

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= ULTRA_VERBOSITY
    printf("SWIFFT_NTT input  after work:  ");
    for (i = 0; i < N_CONSTANT; i++)
    {
        printf("%3u, ", input[i]);
    }
    printf("\n\r");
#endif

    return result;
}
#endif

/*
 * Faster implementation of enumerate_1CFF (~50% faster)
 * Associates any L-bit message to a unique k element subset of {0,...,E-1}
 * Uses pre-computed array of binomial values & eChooseK (~2415 bytes).
 * Computes binomial coefficients needed for next loop iteration
 * by subtracting coefficients from current loop.
 *
 * Python:
 * def new_enumerate(m):
 *     global E, K, echoosek, first_row
 *     # initialize curr_row, next_row
 *     curr_row = [first_row[i] for i in range(0,E-K+2)]
 *     curr_row[len(curr_row)-1] = 0
 *     next_row = [0] * (E-K+2)
 *     a = [0] * K
 *     top_binom = echoosek
 *     sum_binom = top_binom
 *     r = E-K+1
 *     temp = m
 *     for i in range(0,K):
 *         while temp <= sum_binom:
 *             r -= 1
 *             # compute next next_row element
 *             next_row[r] = curr_row[r] - curr_row[r+1]
 *             sum_binom -= curr_row[r]
 *         temp -= sum_binom
 *         a[i] = r + i
 *         sum_binom = curr_row[r]
 *         r = E-K+1
 *         # swaps curr_row, next_row
 *         curr_row, next_row = next_row, curr_row
 *     return a
 */
int new_enumerate(const sots_params *params, BitSequence *subsetE, size_t subsetE_len)
{
    int            result = EXIT_SUCCESS;
    unsigned int   i, j;
    unsigned int   row = 0;
    static DIGIT_T curr_row[NUM_ROWS][NWORDS_FIELD];
    static DIGIT_T next_row[NUM_ROWS][NWORDS_FIELD];
    static DIGIT_T top_binom[NWORDS_FIELD];
    static DIGIT_T sum_binom[NWORDS_FIELD];
    static DIGIT_T temp[NWORDS_FIELD];
    static DIGIT_T temp2[NWORDS_FIELD];       // For swap

#ifdef PERFORM_ARRAY_DIMENSION_VALIDATION
    // Validate dimensions
    if (subsetE_len != K_CONSTANT)
    {
        printf("ERROR: subsetE_len not equal to expected size of %d; actual value = %u\n\r",
               K_CONSTANT,
               subsetE_len);

        return EXIT_FAILURE;
    }
#endif

    // Clear out big digits
    for (i = 0; i < NUM_ROWS; i++)
    {
        mpSetZero(curr_row[i], NWORDS_FIELD);
        mpSetZero(next_row[i], NWORDS_FIELD);
    }
    mpSetZero(top_binom, NWORDS_FIELD);
    mpSetZero(sum_binom, NWORDS_FIELD);
    mpSetZero(temp, NWORDS_FIELD);
    mpSetZero(temp2, NWORDS_FIELD);

    // Zero out subset
    memset(subsetE, 0x0, subsetE_len * sizeof(subsetE[0]));

    mpSetEqual(top_binom, params->eChooseK, NWORDS_FIELD);
    mpSetEqual(sum_binom, top_binom, NWORDS_FIELD);
    mpSetEqual(temp, params->m_bigInt, NWORDS_FIELD);

    for (i = 0; i < NUM_ROWS; i++)
    {
        // Copy first_row to curr_row
        // Python:  curr_row = copy.copy(first_row)
        // curr_row[i] = params->first_row[i]
        mpSetEqual(curr_row[i], params->first_row[i], NWORDS_FIELD);
    }

    // Python:  r = E - K + 1
    row = E_CONSTANT - K_CONSTANT + 1;

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= HIGH_VERBOSITY
    mpPrintHex("top_binom in base 16 = 0x", top_binom, NWORDS_FIELD, "\n\r");
    mpPrintHex("sum_binom in base 16 = 0x", sum_binom, NWORDS_FIELD, "\n\r");
    mpPrintHex("temp in base 16 = 0x", temp, NWORDS_FIELD, "\n\r");
#endif

    // Python:  for i in range(0, K):
    for (i = 0; i < K_CONSTANT; i++)
    {
        /* Python:
         * while temp <= sum_binom:
         *     r -= 1
         *     # compute next next_row element
         *     next_row[r] = curr_row[r] - curr_row[r + 1]
         *     sum_binom  -= curr_row[r]
         */
        // Returns sign of (a-b) as {-1,0,+1}
        int cmpResult = mpCompare(temp, sum_binom, NWORDS_FIELD);

        while (cmpResult <= 0)
        {
            row--;

            // Compute next next_row element
            mpSubtract(next_row[row], curr_row[row], curr_row[row + 1], NWORDS_FIELD);
            mpSubtract(sum_binom, sum_binom, curr_row[row], NWORDS_FIELD);

            cmpResult = mpCompare(temp, sum_binom, NWORDS_FIELD);
        }

        // Python:  temp -= sum_binom
        mpSubtract(temp, temp, sum_binom, NWORDS_FIELD);

        // Python:  a[i]  = r + i
        subsetE[i] = (BitSequence)(row + i);

        // Python:  sum_binom = curr_row[r]
        mpSetEqual(sum_binom, curr_row[row], NWORDS_FIELD);

        // Python:  r = E - K + 1
        row = E_CONSTANT - K_CONSTANT + 1;

        // Python:  curr_row, next_row = next_row, curr_row
        for (j = 0; j < NUM_ROWS; j++)
        {
            mpSetEqual(temp2, next_row[j], NWORDS_FIELD);
            mpSetEqual(next_row[j], curr_row[j], NWORDS_FIELD);
            mpSetEqual(curr_row[j], temp2, NWORDS_FIELD);
        }
    }

    return result;
}
