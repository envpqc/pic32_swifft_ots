/* ************************************************************************** */
/** Descriptive File Name
 *
 * @Company
 *  Company Name
 *
 * @File Name
 *  rand_openssl_aes.h
 *
 * @Summary
 *  Brief description of the file.
 *
 * @Description
 *  Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _RAND_OPENSSL_AES_H    /* Guard against multiple inclusion */
#define _RAND_OPENSSL_AES_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

#include <stdio.h>
#include <crypto/crypto.h>
#include <crypto/src/aes.h>
#include <inttypes.h>
#include <assert.h>


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C"
{
#endif

extern CRYPT_RNG_CTX mcRng;     // declare an external CRYPT_RNG_CTX construct


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Constants                                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
 *  banner.
 */


#define RANDOM_VARS
#define RANDOM8     ((uint8_t)randomplease(1))
#define RANDOM32    ((uint32_t)randomplease(4))
#define RANDOM64    ((uint64_t)randomplease(8))


// *****************************************************************************
// *****************************************************************************
// Section: Interface Functions
// *****************************************************************************
// *****************************************************************************

/*  A brief description of a section can be given directly below the section
 *  banner.
 */

void init_rng();

uint64_t randomplease();

static inline void my_AES_ctr128_encrypt(const unsigned char *in,
                                         unsigned char *out, int len,
                                         CRYPT_AES_CTX *enc, unsigned char counter[AES_BLOCK_SIZE],
                                         unsigned char ecount_buf[AES_BLOCK_SIZE], unsigned int *num)
{
    unsigned int n;
    int          i;

    n = *num;
    while (len--)
    {
        if (n == 0)
        {
            // counter is the plaintext; ecount_buf is the ciphertext -- it is a buffer holding the encrypted counter
            CRYPT_AES_DIRECT_Encrypt(enc, ecount_buf, counter);
            for (i = 15; i >= 8; i--)
            {
                counter[i]++;
                if (counter[i])
                {
                    break;
                }
            }
        }
        *(out++) = *(in++) ^ ecount_buf[n];
        n        = (n + 1) % AES_BLOCK_SIZE;
    }

    *num = n;
}

// *****************************************************************************
/**
 * @Function
 *  int ExampleFunctionName ( int param1, int param2 )
 *
 * @Summary
 *  Brief one-line description of the function.
 *
 * @Description
 *  Full description, explaining the purpose and usage of the function.
 *  <p>
 *  Additional description in consecutive paragraphs separated by HTML
 *  paragraph breaks, as necessary.
 *  <p>
 *  Type "JavaDoc" in the "How Do I?" IDE toolbar for more information on tags.
 *
 * @Precondition
 *  List and describe any required preconditions. If there are no preconditions,
 *  enter "None."
 *
 * @Parameters
 *  @param param1 Describe the first parameter to the function.
 *
 *  @param param2 Describe the second parameter to the function.
 *
 * @Returns
 *  List (if feasible) and describe the return values of the function.
 *  <ul>
 *    <li>1   Indicates an error occurred
 *    <li>0   Indicates an error did not occur
 *  </ul>
 *
 * @Remarks
 *  Describe any special behavior not described above.
 *  <p>
 *  Any additional remarks.
 *
 * @Example
 *  @code
 *  if(ExampleFunctionName(1, 2) == 0)
 *  {
 *      return 3;
 *  }
 */
/*int ExampleFunction(int param1, int param2); */


/* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _RAND_OPENSSL_AES_H */

/* *****************************************************************************
 * End of File
 */
