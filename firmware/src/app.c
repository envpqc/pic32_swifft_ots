/*******************************************************************************
*  MPLAB Harmony Application Source File
*
*  Company:
*   Microchip Technology Inc.
*
*  File Name:
*   app.c
*
*  Summary:
*   This file contains the source code for the MPLAB Harmony application.
*
*  Description:
*   This file contains the source code for the MPLAB Harmony application.  It
*   implements the logic of the application's state machine and it may call
*   API routines of other MPLAB Harmony modules in the system, such as drivers,
*   system services, and middleware.  However, it does not call any of the
*   system interfaces (such as the "Initialize" and "Tasks" functions) of any of
*   the modules in the system or make any assumptions about when those functions
*   are called.  That is the responsibility of the configuration-specific system
*   files.
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
*  Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.
*
*  Microchip licenses to you the right to use, modify, copy and distribute
*  Software only when embedded on a Microchip microcontroller or digital signal
*  controller that is integrated into your product or third party product
*  (pursuant to the sublicense terms in the accompanying license agreement).
*
*  You should refer to the license agreement accompanying this Software for
*  additional information regarding your rights and obligations.
*
*  SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
*  MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
*  IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
*  CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
*  OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
*  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
*  CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
*  SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
*  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include "swifft_ots/params.h"
#include "swifft_ots/swifft-OTS.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************


sots_params PARAMS;
BitSequence PK[E_CONSTANT][N_CONSTANT];
BitSequence SK[E_CONSTANT][M_CONSTANT][N_CONSTANT];

#if defined VERBOSITY_LEVEL && VERBOSITY_LEVEL >= HIGH_VERBOSITY
#   define USE_DETERMINISTIC_DATA true
#else
#   define USE_DETERMINISTIC_DATA false
#endif

// *****************************************************************************
/* Application Data
 *
 * Summary:
 *  Holds application data
 *
 * Description:
 *  This structure holds the application's data.
 *
 * Remarks:
 *  This structure should be initialized by the APP_Initialize function.
 *
 *  Application strings and buffers are be defined outside this structure.
 */

APP_DATA appData;
#define WOLFSSL_DEBUG_MEMORY

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/******************************************************************************
 * void APP_HeartbeatTimerCallback ( uintptr_t context, uint32_t tickCount )
 *
 * Summary:
 * Called back by timer system service. Increments the heartbeat counter, and if
 * at or above the heartbeat threshold sets the heartbeatToggle flag to which
 * is used by the APP_Tasks routine to toggle an LED.
 *
 * Remarks:
 *  context and tickCount parameters ignored.
 */
static void APP_HeartbeatTimerCallback(uintptr_t context, uint32_t alarmCount)
{
    appData.heartbeatCount++;
    if (appData.heartbeatCount >= APP_HEARTBEAT_COUNT_MAX)
    {
        appData.heartbeatCount  = 0;
        appData.heartbeatToggle = true;
    }
}

/******************************************************************************
 * void APP_TimerCallBack ( uintptr_t context, uint32_t tickCount )
 *
 * Summary:
 * Called back by timer system service. Debounces the switch press.
 *
 * Remarks:
 *  context and tickCount parameters ignored.
 */
void APP_TimerCallBack(uintptr_t context, uint32_t tickCount)
{
    /* If we're in a debounce state, check port to see if switch is pressed. */
    if (appData.state == APP_STATE_DEBOUNCE_START)
    {
        appData.timeoutCounter++;

        /* Increment the debounce counter if the switch is pressed
         * Note that when Idle, the switches are pulled high (+3.3V),
         * and when pressed, they are grounded. */
        if (BSP_SWITCH_1StateGet())
        {
            appData.debounce1Counter = 0;
        }
        else
        {
            appData.debounce1Counter++;
        }

        if (BSP_SWITCH_2StateGet())
        {
            appData.debounce2Counter = 0;
        }
        else
        {
            appData.debounce2Counter++;
        }

        if (BSP_SWITCH_3StateGet())
        {
            appData.debounce3Counter = 0;
        }
        else
        {
            appData.debounce3Counter++;
        }

        /* If switch is pressed for APP_DEBOUNCE_COUNT counts, it's real.
         * Switch was pressed for at least 100 msec */

        if (appData.debounce1Counter >= APP_DEBOUNCE_COUNT)
        {
            appData.switch1HasBeenPressed = true;
        }

        if (appData.debounce2Counter >= APP_DEBOUNCE_COUNT)
        {
            appData.switch2HasBeenPressed = true;
        }

        if (appData.debounce3Counter >= APP_DEBOUNCE_COUNT)
        {
            appData.switch3HasBeenPressed = true;
        }
    }
}

/*****************************************************************************
 * void APP_SwitchChangeNoticed ( void )
 *
 * Remarks:
 *  See prototype in app.h.
 */
void APP_SwitchChangeNoticed(void)
{
    uint32_t __attribute__ ((unused)) temp;

    /* Read port to clear mismatch on change notice pins */
    temp = PLIB_PORTS_Read(PORTS_ID_0, APP_CN_PORT_CHANNEL);

    /* Clear the interrupt flag */
    SYS_INT_SourceStatusClear(APP_CN_PORT_INTERRUPT);

    /* Only allow the change to be noticed when in the APP_STATE_SPIN state */
    if (appData.state == APP_STATE_SPIN)
    {
        /* Set flag indicating that a changed has been noticed. */
        appData.changeNoticed = true;
    }
}


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
 * Function:
 *  void APP_Initialize ( void )
 *
 * Remarks:
 *  See prototype in app.h.
 */
void APP_Initialize(void)
{
    /* Place the App state machine in its initial state. */
    appData.state           = APP_STATE_INIT;
    appData.heartbeatTimer  = DRV_HANDLE_INVALID;
    appData.heartbeatCount  = 0;
    appData.heartbeatToggle = true;

    /* Initialize Random Number Generator */
    init_rng();
}


/******************************************************************************
 * Function:
 *  void APP_Tasks ( void )
 *
 * Remarks:
 *  See prototype in app.h.
 */
void APP_Tasks(void)
{
    int             status             = FAILURE;
    static uint64_t appStartTime_ticks = 0;
    static bool     firstPrint         = true;

    /* Signal the application's heartbeat. */
    if (appData.heartbeatToggle == true)
    {
        BSP_LED_1Toggle();
        appData.heartbeatToggle = false;
    }

    /* Check the application's current state */
    switch (appData.state)
    {
    /* Application's initial state */
    case APP_STATE_INIT:
    {
        /* Wait in the Init state until the SYS TMR Module is in running mode */
        if (SYS_STATUS_READY == SYS_TMR_Status(sysObj.sysTmr))
        {
            /* Initialize Heartbeat Timer */
            if (DRV_HANDLE_INVALID == appData.heartbeatTimer)
            {
                appData.heartbeatTimer = DRV_TMR_Open(APP_HEARTBEAT_TMR,
                                                      DRV_IO_INTENT_EXCLUSIVE);

                DRV_TMR_AlarmRegister(appData.heartbeatTimer,
                                      APP_HEARTBEAT_TMR_PERIOD,
                                      APP_HEARTBEAT_TMR_IS_PERIODIC,
                                      (uintptr_t)&appData,
                                      APP_HeartbeatTimerCallback);

                DRV_TMR_Start(appData.heartbeatTimer);
            }

            /* Turn on LED1 to indicate that the application has initialized */
            if (DRV_HANDLE_INVALID != appData.heartbeatTimer)
            {
                appStartTime_ticks = SYS_TMR_TickCountGetLong();

                printf("\n\r\n\r*******************************\n\rStarting pic32_swifft_ots tests\n\r*******************************\n\r");

                APP_SetState(APP_STATE_KNOWN_ANSWER_TEST);
            }
            else
            {
                printf("FAILED to get a valid handle for appData.heartbeatTimer!!\n\r");

                APP_SetState(APP_STATE_ERROR);
            }
        }
        else
        {
            /* Stay in APP_STATE_INIT */
        }

        break;
    }

    case APP_STATE_KNOWN_ANSWER_TEST:
    {
        printf("RUNNING Known Answer Test...\n\r");

        SYS_TMR_ObjectDelete(appData.tmrObj);

        uint64_t startTicks = SYS_TMR_TickCountGetLong();

        /* Perform S-OTS known answer test */
        if (performKnownAnswerTest(&PARAMS,
                                   PK, sizeof(PK) / sizeof(PK[0]),
                                   SK, sizeof(SK) / sizeof(SK[0])) == EXIT_SUCCESS)
        {
            status = SUCCESS;
        }

        APP_GetElapsedTime(startTicks, APP_GetStateName(appData.state));

        if (status < SUCCESS)
        {
            APP_SetState(APP_STATE_ERROR);
        }
        else
        {
            APP_SetState(APP_STATE_INIT_SOTS);
        }
        break;
    }

    case APP_STATE_INIT_SOTS:
    {
        printf("RUNNING S-OTS initialization...\n\r");

        SYS_TMR_ObjectDelete(appData.tmrObj);

        uint64_t startTicks = SYS_TMR_TickCountGetLong();

        /* Initialize S-OTS */
        if (initSOTS(&PARAMS,
                     PK, sizeof(PK) / sizeof(PK[0]),
                     SK, sizeof(SK) / sizeof(SK[0]),
                     USE_DETERMINISTIC_DATA) == EXIT_SUCCESS)
        {
            status = SUCCESS;
        }

        APP_GetElapsedTime(startTicks, APP_GetStateName(appData.state));

        if (status < SUCCESS)
        {
            APP_SetState(APP_STATE_ERROR);
        }
        else
        {
            APP_SetState(APP_STATE_SOTS_SIGNATURE_TEST);
        }
        break;
    }

    case APP_STATE_SOTS_SIGNATURE_TEST:
    {
        printf("RUNNING S-OTS test...\n\r");

        SYS_TMR_ObjectDelete(appData.tmrObj);

        uint64_t startTicks = SYS_TMR_TickCountGetLong();

        /* Perform S-OTS test */
        if (perform_SOTS_SignatureTest(&PARAMS,
                                       PK, sizeof(PK) / sizeof(PK[0]),
                                       SK, sizeof(SK) / sizeof(SK[0]),
                                       USE_DETERMINISTIC_DATA) == EXIT_SUCCESS)
        {
            status = SUCCESS;
        }

        APP_GetElapsedTime(startTicks, APP_GetStateName(appData.state));

        if (status < SUCCESS)
        {
            APP_SetState(APP_STATE_ERROR);
        }
        else
        {
            APP_SetState(APP_STATE_SPIN);
        }
        break;
    }

    case APP_STATE_ERROR:
    {
        BSP_LED_3Off();
        BSP_LED_2Off();
        BSP_LED_1On();
        break;
    }

    case APP_STATE_HANDLE_SWITCH_1:
    {
        printf("Handling Switch 1 press, which right now means doing nothing...\n\r");
        APP_SetState(APP_STATE_SPIN);
        break;
    }

    case APP_STATE_HANDLE_SWITCH_2:
    {
        printf("Handling Switch 2 press, which right now means going back to APP_STATE_INIT_SOTS state\n\r");
        APP_SetState(APP_STATE_INIT_SOTS);
        break;
    }

    case APP_STATE_HANDLE_SWITCH_3:
    {
        printf("Handling Switch 3 press, which right now means doing nothing...\n\r");
        APP_SetState(APP_STATE_SPIN);
        break;
    }

    /* State to create the timer object for periodic alarm */
    case APP_STATE_TIMER_OBJECT_CREATE:
    {
        appData.tmrObj = SYS_TMR_ObjectCreate(APP_DEBOUNCE_CHECK_DELAY, 1, APP_TimerCallBack, SYS_TMR_FLAG_PERIODIC);
        if (SYS_TMR_HANDLE_INVALID != appData.tmrObj)
        {
            printf("System Timer Object successfully created\n\r");

            APP_SetState(APP_STATE_DEBOUNCE_START);
        }
        else
        {
            printf("Failed to create System Timer Object to handle switch debounce\n\r");

            APP_SetState(APP_STATE_ERROR);
        }
        break;
    }

    case APP_STATE_DEBOUNCE_START:
    {
        if (appData.timeoutCounter < APP_DEBOUNCE_TIMEOUT)
        {
            if (appData.switch1HasBeenPressed)
            {
                printf("Switch 1 was successfully pressed while in APP_STATE_DEBOUNCE_START state\n\r");
                appData.changeNoticed = false;

                APP_SetState(APP_STATE_HANDLE_SWITCH_1);
            }

            if (appData.switch2HasBeenPressed)
            {
                printf("Switch 2 was successfully pressed while in APP_STATE_DEBOUNCE_START state\n\r");
                appData.changeNoticed = false;

                APP_SetState(APP_STATE_HANDLE_SWITCH_2);
            }

            if (appData.switch3HasBeenPressed)
            {
                printf("Switch 3 was successfully pressed while in APP_STATE_DEBOUNCE_START state\n\r");
                appData.changeNoticed = false;

                APP_SetState(APP_STATE_HANDLE_SWITCH_3);
            }
        }
        else
        {
            printf("Debounce timed out while in APP_STATE_DEBOUNCE_START state\n\r");
            SYS_TMR_ObjectDelete(appData.tmrObj);

            APP_SetState(APP_STATE_SPIN);
        }
        break;
    }

    case APP_STATE_SPIN:
    {
        BSP_LED_2Off();
        BSP_LED_3On();

        if (firstPrint)
        {
            APP_GetElapsedTime(appStartTime_ticks, "App from start to finish");

            firstPrint = false;
        }

        if (appData.changeNoticed)
        {
            printf("Change noticed while in APP_STATE_SPIN state\n\r");
            /* Start timer and prepare to detect switch press. */
            appData.changeNoticed         = false;
            appData.switch1HasBeenPressed = false;
            appData.switch2HasBeenPressed = false;
            appData.switch3HasBeenPressed = false;
            appData.debounce1Counter      = 0;
            appData.debounce2Counter      = 0;
            appData.debounce3Counter      = 0;
            appData.timeoutCounter        = 0;

            APP_SetState(APP_STATE_TIMER_OBJECT_CREATE);
        }
        break;
    }

    /* The default state should never be executed */
    default:
    {
        printf("SHOULD NOT HAPPEN: Default case reached in APP_Tasks switch statement!\n\r");
        APP_SetState(APP_STATE_ERROR);
        break;
    }
    }
}

/*******************************************************************************
 * Function:
 *  bool APP_SetState ( APP_STATES newState )
 *
 * Remarks:
 *  See prototype in app.h.
 */
bool APP_SetState(APP_STATES newState)
{
    bool transitionOK = true; // Initialize transition flag

    /* Make sure the requested state is valid */
    if ((newState < APP_STATE_INIT) || (newState >= APP_NUM_STATES))
    {
        /* Invalid state specified */
        printf("Unrecognized state commanded!!!: newState=%d\n\r", newState);
        transitionOK = false;
    }
    else if (newState == appData.state)
    {
        /* Invalid state specified */
        printf("Attempted to transition to the same state that we're currently in!\n\r");
        transitionOK = false;
    }

    if (transitionOK)
    {
        /* Make sure the state transition is legal */
        switch (appData.state)
        {
        case APP_STATE_INIT:
        case APP_STATE_INIT_SOTS:
        case APP_STATE_KNOWN_ANSWER_TEST:
        case APP_STATE_SOTS_SIGNATURE_TEST:
        case APP_STATE_HANDLE_SWITCH_1:
        case APP_STATE_HANDLE_SWITCH_2:
        case APP_STATE_HANDLE_SWITCH_3:

            /* Can't transition to STARTUP */
            if (newState == APP_STATE_INIT)
            {
                transitionOK = false;
            }
            break;

        case APP_STATE_ERROR:
            /* The Error state is a one-way trip */
            printf("Can't leave ERROR state once it has been reached!\n\r");
            transitionOK = false;
            break;

        case APP_STATE_SPIN:
            /* Can only transition to:
             * - APP_STATE_SOTS_SIGNATURE_TEST
             * - APP_STATE_TIMER_OBJECT_CREATE
             */
            if ((newState != APP_STATE_SOTS_SIGNATURE_TEST)
                && (newState != APP_STATE_TIMER_OBJECT_CREATE))
            {
                transitionOK = false;
            }
            break;

        case APP_STATE_TIMER_OBJECT_CREATE:
            /* Can only transition to:
             * - APP_STATE_DEBOUNCE_START
             * - APP_STATE_ERROR
             */
            if ((newState != APP_STATE_DEBOUNCE_START)
                && (newState != APP_STATE_ERROR))
            {
                transitionOK = false;
            }
            break;

        case APP_STATE_DEBOUNCE_START:
            /* Can only transition to:
             * - APP_STATE_SOTS_SIGNATURE_TEST
             * - APP_STATE_HANDLE_SWITCH_1
             * - APP_STATE_HANDLE_SWITCH_2
             * - APP_STATE_HANDLE_SWITCH_3
             * - APP_STATE_SPIN
             */
            if ((newState != APP_STATE_SOTS_SIGNATURE_TEST)
                && (newState != APP_STATE_HANDLE_SWITCH_1)
                && (newState != APP_STATE_HANDLE_SWITCH_2)
                && (newState != APP_STATE_HANDLE_SWITCH_3)
                && (newState != APP_STATE_SPIN))
            {
                transitionOK = false;
            }
            break;

        default:
            /* App is in an unknown state */
            transitionOK = false;
            break;
        }
    }

    /* Set the new state if it's allowable */
    if (transitionOK)
    {
        if (newState == APP_STATE_SPIN)
        {
            /* Ensure that timer is deleted on transition to APP_STATE_SPIN */
            SYS_TMR_ObjectDelete(appData.tmrObj);
        }

        printf("\n\r\n\rTransitioned from %s to %s\n\r",
               APP_GetStateName(appData.state),
               APP_GetStateName(newState));
        appData.state = newState;
    }
    /* Requested state transition not allowed */
    else
    {
        printf("\n\r\n\rAPP_SetState: Illegal state transition from %s to %s; transitioning to APP_STATE_ERROR\n\r",
               APP_GetStateName(appData.state),
               APP_GetStateName(newState));
        appData.state = APP_STATE_ERROR;
    }

    /* Return the transition flag to the caller */
    return transitionOK;
}

/*******************************************************************************
 * Function:
 *  void APP_GetStateName ( APP_STATES newState )
 *
 * Remarks:
 *  See prototype in app.h.
 */
char *APP_GetStateName(APP_STATES state)
{
    switch (state)
    {
    case APP_STATE_INIT:

        return "APP_STATE_INIT";

    case APP_STATE_KNOWN_ANSWER_TEST:

        return "APP_STATE_KNOWN_ANSWER_TEST";

    case APP_STATE_INIT_SOTS:

        return "APP_STATE_INIT_SOTS";

    case APP_STATE_SOTS_SIGNATURE_TEST:

        return "APP_STATE_SOTS_SIGNATURE_TEST";

    case APP_STATE_ERROR:

        return "APP_STATE_ERROR";

    case APP_STATE_HANDLE_SWITCH_1:

        return "APP_STATE_HANDLE_SWITCH_1";

    case APP_STATE_HANDLE_SWITCH_2:

        return "APP_STATE_HANDLE_SWITCH_2";

    case APP_STATE_HANDLE_SWITCH_3:

        return "APP_STATE_HANDLE_SWITCH_3";

    case APP_STATE_TIMER_OBJECT_CREATE:

        return "APP_STATE_TIMER_OBJECT_CREATE";

    case APP_STATE_DEBOUNCE_START:

        return "APP_STATE_DEBOUNCE_START";

    case APP_STATE_SPIN:

        return "APP_STATE_SPIN";

    default:

        return "INVALID APP STATE!";
    }
}

/*******************************************************************************
 * Function:
 *  void APP_GetElapsedTime ( uint64_t startTime_ticks, char *eventName )
 *
 * Remarks:
 *  See prototype in app.h.
 */
uint64_t APP_GetElapsedTime(uint64_t startTime_ticks, char *eventName)
{
    static double tickFrequency_Hz = 1.0;
    uint64_t      timeDiff_ticks   = 0;
    double        timeDiff_sec     = 0.0;

    if (tickFrequency_Hz < 2.0)
    {
        tickFrequency_Hz = SYS_TMR_TickCounterFrequencyGet();
    }

    timeDiff_ticks = SYS_TMR_TickCountGetLong() - startTime_ticks;
    timeDiff_sec   = (double)timeDiff_ticks / tickFrequency_Hz;

    printf("Time spent in %s: ticks = %llu; sec = %f; msec = %f\n\r",
           eventName,
           timeDiff_ticks,
           timeDiff_sec,
           timeDiff_sec * 1000.0);

    return timeDiff_ticks;
}

/*******************************************************************************
 * End of File
 */
