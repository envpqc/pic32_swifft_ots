set -x

find . -name \*.h -print -o -name \*.cpp -print -o -name \*.c -print -o -name \*.cc -print | grep -v system_config | xargs uncrustify -c pic32Uncrustify.cfg --no-backup

