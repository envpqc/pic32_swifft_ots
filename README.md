# README #

This repo contains an MPLAB Harmony project for building SWIFFT-OTS for the PIC32.

### How do I get set up? ###

1. cd into ~/microchip/harmony/v2\_04/apps (or similar directory)
1. git clone this repo
1. open the project from MPLAB IDE X

### Sample Output ###

```
*******************************
Starting pic32_swifft_ots tests
*******************************


Transitioned from APP_STATE_INIT to APP_STATE_KNOWN_ANSWER_TEST
RUNNING Known Answer Test...
Performing Known Answer Test...

Note that the A parameter will be randomized before each keypair is generated
Known Answer Test timing:  0.244755 seconds; 244.755249 msec; 244755.234375 usec

Known Answer Test completed successfully!

##################################################################################################

Time spent in APP_STATE_KNOWN_ANSWER_TEST: ticks = 268; sec = 0.267732; msec = 267.732269


Transitioned from APP_STATE_KNOWN_ANSWER_TEST to APP_STATE_INIT_SOTS
RUNNING S-OTS initialization...
Note that the A parameter will be randomized before each keypair is generated
Note that the keypair will be generated before each signature.
Time spent in APP_STATE_INIT_SOTS: ticks = 13; sec = 0.012987; msec = 12.987013


Transitioned from APP_STATE_INIT_SOTS to APP_STATE_SOTS_SIGNATURE_TEST
RUNNING S-OTS test...
Output using Envieta multiple precision code:

Testing 1000 signatures..

Average time spent in sots_core_generate_keypair across 1000 signatures:  0.330575 seconds; 330.575256 msec; 330575.218750 usec
Total   time spent in sots_core_generate_keypair across 1000 signatures:  330.575226 seconds; 330575.250000 msec; 330575232.000000 usec
Average time spent in sots_core_sign across 1000 signatures:              0.027298 seconds; 27.297873 msec; 27297.871094 usec
Total   time spent in sots_core_sign across 1000 signatures:              27.297873 seconds; 27297.873047 msec; 27297872.000000 usec
Average time spent in sots_core_verify_signature across 1000 signatures:  0.020949 seconds; 20.949129 msec; 20949.128906 usec
Total   time spent in sots_core_verify_signature across 1000 signatures:  20.949127 seconds; 20949.128906 msec; 20949128.000000 usec
Average time per signature over 1000 signatures:                          0.378888 seconds; 378.888153 msec; 378888.125000 usec
Total time spent performing 1000 signatures:                              378.888123 seconds; 378888.156250 msec; 378888128.000000 usec

Test Complete!
Time spent in APP_STATE_SOTS_SIGNATURE_TEST: ticks = 379366; sec = 378.987000; msec = 378987.000000


Transitioned from APP_STATE_SOTS_SIGNATURE_TEST to APP_STATE_SPIN
Time spent in App from start to finish: ticks = 379711; sec = 379.331665; msec = 379331.656250
```

### Who do I talk to? ###

* John Ratz (john.ratz@envieta.com)
